package com.playminity.seawars.minityshop;

import lombok.Getter;
import me.kombustorlp.gameapi.libraries.material.MaterialInfo;
import me.kombustorlp.gameapi.libraries.material.PotionInfo;
import me.kombustorlp.gameapi.minityshop.api.Description;
import me.kombustorlp.gameapi.minityshop.api.IArticle;
import me.kombustorlp.gameapi.minityshop.api.ICategory;
import me.kombustorlp.gameapi.minityshop.api.Preview;
import me.kombustorlp.gameapi.misc.User;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Created by André on 15.10.2016.
 */
public enum KillEffects implements IArticle {

    SPEED_2( "Speed | 2 seconds", new Desc( "speed", 2 ), 3000, new PotionInfo( 2, PotionEffectType.SPEED ), 2 * 20, 0 ),
    SPEED_4( "Speed | 4 seconds", new Desc( "speed", 4 ), 4000, new PotionInfo( 4, PotionEffectType.SPEED ), 4 * 20, 0, SPEED_2 ),
    SPEED_6( "Speed | 6 seconds", new Desc( "speed", 6 ), 5000, new PotionInfo( 6, PotionEffectType.SPEED ), 6 * 20, 0, SPEED_4 ),
    SPEED2_2( "Speed 2 | 2 seconds", new Desc( "speed 2", 2 ), 4000, new PotionInfo( 2, PotionEffectType.SPEED ), 2 * 20, 1, SPEED_4 ),
    SPEED2_4( "Speed 2 | 4 seconds", new Desc( "speed 2", 4 ), 5000, new PotionInfo( 4, PotionEffectType.SPEED ), 4 * 20, 1, SPEED2_2 ),
    SPEED2_6( "Speed 2 | 6 seconds", new Desc( "speed 2", 6 ), 6000, new PotionInfo( 6, PotionEffectType.SPEED ), 6 * 20, 1, SPEED2_4 ),

    REGENERATION_3( "Regeneration | 3 seconds", new Desc( "regeneration", 3 ), 6000, new PotionInfo( 3, PotionEffectType.REGENERATION ), 4 * 20, 1 ),
    REGENERATION_6( "Regeneration | 6 seconds", new Desc( "regeneration", 6 ), 7000, new PotionInfo( 6, PotionEffectType.REGENERATION ), 6 * 20, 1, REGENERATION_3 ),
    REGENERATION_9( "Regeneration | 9 seconds", new Desc( "regeneration", 9 ), 8000, new PotionInfo( 9, PotionEffectType.REGENERATION ), 9 * 20, 1, REGENERATION_6 ),;

    @Getter
    private String name;
    @Getter
    private ICategory category = ShopCategories.KILL_EFFECTS;
    @Getter
    private Description description;
    @Getter
    private boolean premium;
    @Getter
    private int price;
    @Getter
    private MaterialInfo materialInfo;
    @Getter
    private IArticle[] dependencies;
    @Getter
    private Preview preview;

    private int duration;
    private int amplifier;

    KillEffects ( String name, Description description, int price, MaterialInfo materialInfo, int duration, int amplifier, IArticle... dependencies ) {

        this( name, description, false, price, materialInfo, null, duration, amplifier, dependencies );
    }

    KillEffects ( String name, Description description, boolean premium, int price, MaterialInfo materialInfo, Preview preview, int duration, int amplifier, IArticle... dependencies ) {

        this.name = name;
        this.description = description;
        this.premium = premium;
        this.price = price;
        this.materialInfo = materialInfo;
        this.preview = preview;
        this.dependencies = dependencies;
        this.duration = duration;
        this.amplifier = amplifier;
    }

    public static PotionEffect getPotionEffect ( User user ) {

        String active = user.getActive( ShopCategories.KILL_EFFECTS );
        if ( active == null ) return null;

        KillEffects effect = valueOf( active );
        return new PotionEffect( ( ( PotionInfo ) effect.materialInfo ).getDisplayType( ), effect.duration, effect.amplifier, false, false );
    }

    private static class Desc extends Description {

        public Desc ( String effect, int seconds ) {

            super( "You will get the " + effect + " effect", "for " + seconds + " seconds if you kill a player" );
        }

    }

}
