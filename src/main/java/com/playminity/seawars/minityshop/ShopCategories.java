package com.playminity.seawars.minityshop;

import lombok.AllArgsConstructor;
import lombok.Getter;
import me.kombustorlp.gameapi.libraries.material.PotionInfo;
import me.kombustorlp.gameapi.minityshop.api.CategoryInfo;
import me.kombustorlp.gameapi.minityshop.api.ICategory;
import org.bukkit.potion.PotionEffectType;

/**
 * Created by André on 15.10.2016.
 */
@AllArgsConstructor
public enum ShopCategories implements ICategory {

    KILL_EFFECTS( new CategoryInfo( "Kill effects", new PotionInfo( PotionEffectType.HEALTH_BOOST ) ) ),;

    @Getter
    private CategoryInfo categoryInfo;

}
