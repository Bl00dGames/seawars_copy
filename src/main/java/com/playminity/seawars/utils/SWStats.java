package com.playminity.seawars.utils;

import com.playminity.minigameframework.modules.utils.IStats;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by André on 14.10.2016.
 */
@AllArgsConstructor
public enum SWStats implements IStats {

    POINTS( "points" ),
    DEATHS( "deaths" ),
    KILLS( "kills" ),
    GAMESPLAYED( "gamesplayed" ),
    WINS( "wins" ),
    GOLD( "gold" ),
    PIRATES( "pirates" ),
    SAILORS( "sailors" ),;

    @Getter
    private String name;
}
