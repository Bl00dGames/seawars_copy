package com.playminity.seawars.utils;

import com.playminity.minigameframework.handler.GameHandler;
import com.playminity.rest.internalapi.routes.utils.Game;
import com.playminity.seawars.Data;
import com.playminity.seawars.SWGameHandler;
import com.playminity.seawars.SeaWars;
import de.supereg.mysql.MySql;
import jdbchelper.QueryResult;
import lombok.Getter;
import me.kombustorlp.gameapi.libraries.ItemUtils;
import me.kombustorlp.gameapi.libraries.inventories.ClickEvent;
import me.kombustorlp.gameapi.libraries.inventories.Inventories;
import me.kombustorlp.gameapi.libraries.inventories.InventoryListener;
import me.kombustorlp.gameapi.misc.User;
import me.kombustorlp.gameapi.utils.ProtocolAPI;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by André on 11.11.2016.
 */
public class TutorialManager {

    public static Map< User, Location > locationMap = new HashMap<>( );
    @Getter
    private List< User > userList = new ArrayList<>( );
    private Plugin plugin;

    public TutorialManager ( Plugin plugin ) {

        this.plugin = plugin;
        MySql.updateSync( "CREATE TABLE IF NOT EXISTS " + Game.SEAWARS.getName( ) + "(uuid varchar(225), name varchar(16), setting_tutorial boolean, PRIMARY KEY(uuid, name));" );
    }

    /**
     * Goldcollection
     * Pirate_Upgrade
     * Sailor_Upgrade
     * Hostage
     */

    public void addUserToQueue ( User user ) {

        if ( userList.contains( user ) ) {

            user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "tutorial.start.running" );
            return;
        }

        Bukkit.getOnlinePlayers( ).forEach( player -> {
            user.getPlayer( ).hidePlayer( player );
        } );

        userList.add( user );
        user.getPlayer( ).setAllowFlight( true );
        user.getPlayer( ).setFlying( true );

        locationMap.put( user, user.getPlayer( ).getLocation( ) );

        ProtocolAPI.sendTitle( user.getPlayer( ), user.getLanguage( ).get( "tutorial.title" ), user.getLanguage( ).get( "tutorial.subtitle" ), 10, 20, 10 );

        user.sendMessage( "§5=====================================" );

        new BukkitRunnable( ) {

            int i = 1;

            @Override
            public void run ( ) {

                if ( i == 6 || ! user.getPlayer( ).isOnline( ) || user.getPlayer( ) == null || SWGameHandler.getInstance( ).getCurrentPhase( ) != SeaWars.getInstance( ).getTutorialPhase( ) ) {
                    removeUserFromQueue( user );
                    cancel( );
                }

                user.getPlayer( ).teleport( Data.getTutorialArena( ).getData( ).getSpawn( i ).getLocation( ) );
                user.sendMessage( "", Data.getTutorialArena( ).getData( ).getMessageIndex( i ) );
                user.sendMessage( "§5=====================================" );
                i++;

            }
        }.runTaskTimer( plugin, 0, 12 * 20 );
    }

    public void removeUserFromQueue ( User user ) {

        userList.remove( user );
        user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "tutorial.remove.user" );

        user.getPlayer( ).setAllowFlight( false );
        user.getPlayer( ).setFlying( false );

        user.getPlayer( ).teleport( locationMap.get( user ) );
        locationMap.remove( user );
        GameHandler.getBaseInstance( ).setMoveAllowed( user.getPlayer( ), false );
        SeaWars.getInstance( ).getTutorialManager( ).setPlayTutorial( user, false );

        Bukkit.getOnlinePlayers( ).forEach( player -> {
            user.getPlayer( ).showPlayer( player );
        } );
    }

    public boolean isInTutorial ( User user ) {

        return userList.contains( user );
    }

    public void openSettings ( User user ) {

        Inventory inventory = Inventories.newListenableInventory( "§6§l" + user.getLanguage( ).get( "inventory.settings.title" ), 9, new InventoryListener( ) {

            @Override
            public void click ( ClickEvent clickEvent, User user ) {

                clickEvent.setCanceled( true );

                if ( playTutorial( user ) ) {

                    clickEvent.getInventory( ).setItem( 4, ItemUtils.createItemStack( Material.REDSTONE_COMPARATOR, user.getLanguage( ).get( "items.settings.tutorial.off" ) ) );
                    setPlayTutorial( user, false );
                } else {

                    clickEvent.getInventory( ).setItem( 4, ItemUtils.createItemStack( Material.REDSTONE_COMPARATOR, user.getLanguage( ).get( "items.settings.tutorial.on" ) ) );
                    setPlayTutorial( user, true );
                }
            }
        } );

        for ( int i = 0; i < 9; i++ ) {

            inventory.setItem( i, ItemUtils.createItemStack( Material.STAINED_GLASS_PANE, " " ) );
        }

        if ( playTutorial( user ) ) {

            inventory.setItem( 4, ItemUtils.createItemStack( Material.REDSTONE_COMPARATOR, user.getLanguage( ).get( "items.settings.tutorial.on" ) ) );
        } else {

            inventory.setItem( 4, ItemUtils.createItemStack( Material.REDSTONE_COMPARATOR, user.getLanguage( ).get( "items.settings.tutorial.off" ) ) );
        }


        user.getPlayer( ).openInventory( inventory );
    }

    public boolean hasSetting ( User user ) {

        QueryResult queryResult = MySql.query( "SELECT * FROM `" + Game.SEAWARS.getName( ) + "` WHERE `uuid`='" + user.getUUID( ) + "'" );
        if ( queryResult.next( ) ) {

            return true;
        }

        return false;
    }

    public boolean playTutorial ( User user ) {

        QueryResult queryResult = MySql.query( "SELECT setting_tutorial FROM `" + Game.SEAWARS.getName( ) + "` WHERE `uuid`='" + user.getUUID( ) + "'" );
        if ( queryResult.next( ) ) {

            return queryResult.getBoolean( "setting_tutorial" );
        }

        return false;
    }

    public void setPlayTutorial ( User user, boolean play ) {

        MySql.update( "INSERT INTO " + Game.SEAWARS.getName( ) + "(`uuid`, `name`, `setting_tutorial`) VALUES ('" + user.getUUID( ) +
                "', '" + user.getName( ) + "'," + play + ") " + "ON DUPLICATE KEY UPDATE setting_tutorial=" +
                play + ", name='" + user.getName( ) + "';" );
    }
}
