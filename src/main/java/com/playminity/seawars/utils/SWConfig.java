package com.playminity.seawars.utils;

import com.playminity.minigameframework.utils.JoinPrivilege;
import lombok.Getter;

/**
 * Created by Andr� on 28.09.2016.
 */
public class SWConfig {

    @Getter
    private int minPlayers = 10, teamSize = 5, teleportSeconds = 10;

    @Getter
    private int teleportTime = 60, beginTime = 20, ingameTime = 60 * 10;

    @Getter
    private JoinPrivilege joinPrivilege = JoinPrivilege.TEAM;
}
