package com.playminity.seawars.utils;

import lombok.Getter;
import me.kombustorlp.gameapi.achievements.IAchievement;
import me.kombustorlp.gameapi.achievements.RewardType;

/**
 * Created by Andr� on 22.10.2016.
 */
public enum SWAchievements implements IAchievement {

    FIRSTGAME( "First game", "Play one game", RewardType.MINITIES, 15 ),
    FIRSTWIN( "First win", "Win one game", RewardType.MINITIES, 50 ),
    FIRSTKILL( "First kill", "Kill one player", RewardType.MINITIES, 20 ),
    VILLAGER_KILLS( "Kill all villagers", "Kill all villagers", RewardType.MINITIES, 35 );


    @Getter
    private final String name;
    @Getter
    private final String description;
    @Getter
    private final RewardType rewardType;
    @Getter
    private final int reward;
    @Getter
    private boolean countable;
    @Getter
    private int countGoal;

    SWAchievements ( String name, String description ) {

        this( name, description, RewardType.NONE, 0 );
    }

    SWAchievements ( String name, String description, RewardType rewardType, int reward ) {

        this.name = name;
        this.description = description;
        this.rewardType = rewardType;
        this.reward = reward;
    }

    SWAchievements ( String name, String description, RewardType rewardType, int reward, int countGoal ) {

        this( name, description, rewardType, reward );
        this.countable = true;
        this.countGoal = countGoal;
    }
}
