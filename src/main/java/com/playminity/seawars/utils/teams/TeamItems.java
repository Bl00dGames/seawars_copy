package com.playminity.seawars.utils.teams;

import com.playminity.minigameframework.countdown.Countdown;
import com.playminity.minigameframework.modules.utils.GameTeam;
import com.playminity.seawars.Data;
import com.playminity.seawars.SWGameHandler;
import com.playminity.seawars.arenasystem.SWArenaHandler;
import me.kombustorlp.gameapi.GameAPI;
import me.kombustorlp.gameapi.languagesystem.Language;
import me.kombustorlp.gameapi.libraries.ItemUtils;
import me.kombustorlp.gameapi.misc.User;
import me.kombustorlp.gameapi.utils.InvUtil;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by André on 30.10.2016.
 */
public class TeamItems {

    public static void giveTeamItems ( Player player ) {

        User user = GameAPI.getUser( player );
        Language language = user.getLanguage( );

        if ( SWGameHandler.getInstance( ).getTeamModule( ).getTeam( player ).getIndex( ) == 1 ) {

            user.getPlayer( ).getInventory( ).remove( Material.SHEARS );
            user.getPlayer( ).getInventory( ).remove( Material.WOOD_SPADE );
            user.getPlayer( ).getInventory( ).remove( Material.STONE_SWORD );
            user.getPlayer( ).getInventory( ).remove( Material.WOOD_AXE );
            user.getPlayer( ).getInventory( ).remove( Material.WOOD_SWORD );
            user.getPlayer( ).getInventory( ).remove( Material.BLAZE_ROD );
            user.getPlayer( ).getInventory( ).remove( Material.IRON_AXE );
            user.getPlayer( ).getInventory( ).remove( Material.COMPASS );

            int latestUpgrade = SWGameHandler.getInstance( ).getLatestUpgrade( );

            switch (latestUpgrade){

                case 0:

                    ItemStack itemStack = ItemUtils.createItemStack( Material.SHEARS, language.get( "items.sailors.pitchfork" ) );
                    itemStack.addUnsafeEnchantment( Enchantment.DAMAGE_ALL, 1 );

                    user.getPlayer( ).getInventory( ).addItem( itemStack );
                    user.getPlayer( ).getInventory( ).setHelmet( createLeatherArmorItem( Material.LEATHER_HELMET, Color.BLUE, null, player, language.get( "items.sailors.leather_helm" ) ) );
                    user.getPlayer( ).getInventory( ).setChestplate( createLeatherArmorItem( Material.LEATHER_CHESTPLATE, Color.BLUE, null, player, language.get( "items.sailors.leather_chestplate" ) ) );
                    user.getPlayer( ).getInventory( ).setLeggings( createLeatherArmorItem( Material.LEATHER_LEGGINGS, Color.BLUE, null, player, language.get( "items.sailors.leather_leggings" ) ) );
                    user.getPlayer( ).getInventory( ).setBoots( createLeatherArmorItem( Material.LEATHER_BOOTS, Color.BLUE, null, player, language.get( "items.sailors.leather_boots" ) ) );

                case 1:

                    ItemStack itemStack = ItemUtils.createItemStack( Material.SHEARS, language.get( "items.sailors.pitchfork" ) );
                    itemStack.addUnsafeEnchantment( Enchantment.DAMAGE_ALL, 1 );

                    user.getPlayer( ).getInventory( ).addItem( itemStack );
                    user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.WOOD_SPADE, language.get( "items.sailors.colt" ) ) );
                    user.getPlayer( ).getInventory( ).setHelmet( createLeatherArmorItem( Material.LEATHER_HELMET, Color.BLUE, null, player, language.get( "items.sailors.leather_helm" ) ) );
                    user.getPlayer( ).getInventory( ).setChestplate( ItemUtils.createItemStack( Material.GOLD_CHESTPLATE, language.get( "items.sailors.gold_chestplate" ) ) );
                    user.getPlayer( ).getInventory( ).setLeggings( createLeatherArmorItem( Material.LEATHER_LEGGINGS, Color.BLUE, null, player, language.get( "items.sailors.leather_leggings" ) ) );
                    user.getPlayer( ).getInventory( ).setBoots( createLeatherArmorItem( Material.LEATHER_BOOTS, Color.BLUE, null, player, language.get( "items.sailors.leather_boots" ) ) );

                case 2:

                    user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.WOOD_SPADE, language.get( "items.sailors.colt" ) ) );
                    user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.STONE_SWORD, language.get( "items.sailors.knife" ) ) );
                    user.getPlayer( ).getInventory( ).setHelmet( createLeatherArmorItem( Material.LEATHER_HELMET, Color.BLUE, null, player, language.get( "items.sailors.leather_helm" ) ) );
                    user.getPlayer( ).getInventory( ).setChestplate( ItemUtils.createItemStack( Material.GOLD_CHESTPLATE, language.get( "items.sailors.gold_chestplate" ) ) );
                    user.getPlayer( ).getInventory( ).setLeggings( ItemUtils.createItemStack( Material.GOLD_LEGGINGS, language.get( "items.sailors.gold_leggings" ) ) );
                    user.getPlayer( ).getInventory( ).setBoots( createLeatherArmorItem( Material.LEATHER_BOOTS, Color.BLUE, null, player, language.get( "items.sailors.leather_boots" ) ) );

                case 3:

                    user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.STONE_SWORD, language.get( "items.sailors.knife" ) ) );
                    user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.WOOD_AXE, language.get( "items.sailors.musket" ) ) );
                    user.getPlayer( ).getInventory( ).setHelmet( createLeatherArmorItem( Material.LEATHER_HELMET, Color.BLUE, null, player, language.get( "items.sailors.leather_helm" ) ) );
                    user.getPlayer( ).getInventory( ).setChestplate( ItemUtils.createItemStack( Material.GOLD_CHESTPLATE, language.get( "items.sailors.gold_chestplate" ) ) );
                    user.getPlayer( ).getInventory( ).setLeggings( ItemUtils.createItemStack( Material.IRON_LEGGINGS, language.get( "items.sailors.iron_leggings" ) ) );
                    user.getPlayer( ).getInventory( ).setBoots( ItemUtils.createItemStack( Material.GOLD_BOOTS, language.get( "items.sailors.gold_boots" ) ) );

                case 4:

                    user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.WOOD_AXE, language.get( "items.sailors.musket" ) ) );
                    user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.WOOD_SWORD, language.get( "items.sailors.dagger" ) ) );
                    user.getPlayer( ).getInventory( ).setHelmet( ItemUtils.createItemStack( Material.IRON_HELMET, language.get( "items.sailors.iron_helm" ) ) );
                    user.getPlayer( ).getInventory( ).setChestplate( ItemUtils.createItemStack( Material.GOLD_CHESTPLATE, language.get( "items.sailors.gold_chestplate" ) ) );
                    user.getPlayer( ).getInventory( ).setLeggings( ItemUtils.createItemStack( Material.IRON_LEGGINGS, language.get( "items.sailors.iron_leggings" ) ) );
                    user.getPlayer( ).getInventory( ).setBoots( ItemUtils.createItemStack( Material.GOLD_BOOTS, language.get( "items.sailors.gold_boots" ) ) );

                case 5:

                    user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.WOOD_SWORD, language.get( "items.sailors.dagger" ) ) );
                    user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.BLAZE_ROD, language.get( "items.sailors.shotgun" ) ) );
                    user.getPlayer( ).getInventory( ).setHelmet( ItemUtils.createItemStack( Material.IRON_HELMET, language.get( "items.sailors.iron_helm" ) ) );
                    user.getPlayer( ).getInventory( ).setChestplate( ItemUtils.createItemStack( Material.IRON_CHESTPLATE, language.get( "items.sailors.iron_chestplate" ) ) );
                    user.getPlayer( ).getInventory( ).setLeggings( ItemUtils.createItemStack( Material.IRON_LEGGINGS, language.get( "items.sailors.iron_leggings" ) ) );
                    user.getPlayer( ).getInventory( ).setBoots( ItemUtils.createItemStack( Material.IRON_BOOTS, language.get( "items.sailors.iron_boots" ) ) );

                case 6:

                    user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.WOOD_SWORD, language.get( "items.sailors.dagger" ) ) );
                    user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.IRON_AXE, language.get( "items.sailors.axe.throwing" ) ) );
                    user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.BLAZE_ROD, language.get( "items.sailors.shotgun" ) ) );
                    user.getPlayer( ).getInventory( ).setHelmet( ItemUtils.createItemStack( Material.DIAMOND_HELMET, language.get( "items.sailors.diamond_helm" ) ) );
                    user.getPlayer( ).getInventory( ).setChestplate( ItemUtils.createItemStack( Material.IRON_CHESTPLATE, language.get( "items.sailors.iron_chestplate" ) ) );
                    user.getPlayer( ).getInventory( ).setLeggings( ItemUtils.createItemStack( Material.IRON_LEGGINGS, language.get( "items.sailors.iron_leggings" ) ) );
                    user.getPlayer( ).getInventory( ).setBoots( ItemUtils.createItemStack( Material.DIAMOND_BOOTS, language.get( "items.sailors.diamond_boots" ) ) );
            }

            user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.COMPASS, language.get( "items.sailors.compass" ) ) );

            player.setCompassTarget( Data.getVotedArena( ).getData( ).getSpawn( "hostage", 1 ).getLocation( ) );
        } else {

            user.getPlayer( ).getInventory( ).remove( Material.WOOD_HOE );
            user.getPlayer( ).getInventory( ).remove( Material.STONE_SWORD );
            user.getPlayer( ).getInventory( ).remove( Material.STONE_SPADE );
            user.getPlayer( ).getInventory( ).remove( Material.IRON_SWORD );
            user.getPlayer( ).getInventory( ).remove( Material.IRON_SPADE );
            user.getPlayer( ).getInventory( ).remove( Material.EGG );

            if ( isPercentageBetween( 0, 9, false, true ) ) {

                ItemStack itemStack = ItemUtils.createItemStack( Material.WOOD_HOE, language.get( "items.pirates.hook" ) );
                itemStack.addUnsafeEnchantment( Enchantment.DAMAGE_ALL, 1 );

                user.getPlayer( ).getInventory( ).addItem( itemStack );
                user.getPlayer( ).getInventory( ).setHelmet( createLeatherArmorItem( Material.LEATHER_HELMET, Color.BLACK, null, player, language.get( "items.pirates.leather_helm.torn" ) ) );
                user.getPlayer( ).getInventory( ).setChestplate( createLeatherArmorItem( Material.LEATHER_CHESTPLATE, Color.BLACK, null, player, language.get( "items.pirates.leather_chestplate.torn" ) ) );
                user.getPlayer( ).getInventory( ).setLeggings( createLeatherArmorItem( Material.LEATHER_LEGGINGS, Color.BLACK, null, player, language.get( "items.pirates.leather_leggings.torn" ) ) );
                user.getPlayer( ).getInventory( ).setBoots( createLeatherArmorItem( Material.LEATHER_BOOTS, Color.BLACK, null, player, language.get( "items.pirates.leather_boots.torn" ) ) );
            }

            if ( isPercentageBetween( 10, 19, false, true ) ) {

                user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.STONE_SWORD, language.get( "items.pirates.knife" ) ) );
                user.getPlayer( ).getInventory( ).setHelmet( createLeatherArmorItem( Material.LEATHER_HELMET, Color.RED, Enchantment.PROTECTION_ENVIRONMENTAL, player, language.get( "items.pirates.leather_helm" ) ) );
                user.getPlayer( ).getInventory( ).setChestplate( createLeatherArmorItem( Material.LEATHER_CHESTPLATE, Color.RED, Enchantment.PROTECTION_ENVIRONMENTAL, player, language.get( "language.get(items.pirates.leather_chestplate" ) ) );
                user.getPlayer( ).getInventory( ).setLeggings( createLeatherArmorItem( Material.LEATHER_LEGGINGS, Color.RED, Enchantment.PROTECTION_ENVIRONMENTAL, player, language.get( "items.pirates.leather_leggings" ) ) );
                user.getPlayer( ).getInventory( ).setBoots( createLeatherArmorItem( Material.LEATHER_BOOTS, Color.RED, Enchantment.PROTECTION_ENVIRONMENTAL, player, language.get( "items.pirates.leather_boots" ) ) );
            }

            if ( isPercentageBetween( 20, 39, false, true ) ) {

                user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.STONE_SWORD, language.get( "items.pirates.knife" ) ) );
                user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.STONE_SPADE, language.get( "items.pirates.colt.little" ) ) );
                user.getPlayer( ).getInventory( ).setHelmet( createLeatherArmorItem( Material.LEATHER_HELMET, Color.RED, Enchantment.PROTECTION_ENVIRONMENTAL, player, language.get( "items.pirates.leather_helm" ) ) );
                user.getPlayer( ).getInventory( ).setChestplate( ItemUtils.createItemStack( Material.IRON_CHESTPLATE, language.get( "items.pirates.iron_chestplate" ) ) );
                user.getPlayer( ).getInventory( ).setLeggings( createLeatherArmorItem( Material.LEATHER_LEGGINGS, Color.RED, Enchantment.PROTECTION_ENVIRONMENTAL, player, language.get( "items.pirates.leather_leggings" ) ) );
                user.getPlayer( ).getInventory( ).setBoots( ItemUtils.createItemStack( Material.IRON_BOOTS, language.get( "items.pirates.iron_boots" ) ) );
            }

            if ( isPercentageBetween( 40, 59, false, true ) ) {

                user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.STONE_SPADE, language.get( "items.pirates.colt.little" ) ) );
                user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.IRON_SWORD, language.get( "items.pirates.saber" ) ) );
                user.getPlayer( ).getInventory( ).setHelmet( ItemUtils.createItemStack( Material.IRON_HELMET, language.get( "items.pirates.iron_helm" ) ) );
                user.getPlayer( ).getInventory( ).setChestplate( ItemUtils.createItemStack( Material.IRON_CHESTPLATE, language.get( "items.pirates.iron_chestplate" ) ) );
                user.getPlayer( ).getInventory( ).setLeggings( ItemUtils.createItemStack( Material.IRON_LEGGINGS, language.get( "items.pirates.iron_leggings" ) ) );
                user.getPlayer( ).getInventory( ).setBoots( ItemUtils.createItemStack( Material.IRON_BOOTS, language.get( "items.pirates.iron_boots" ) ) );
            }

            if ( isPercentageBetween( 60, 79, false, true ) ) {

                user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.IRON_SWORD, language.get( "items.pirates.saber" ) ) );
                user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.IRON_SPADE, language.get( "items.pirates.colt.big" ) ) );
                user.getPlayer( ).getInventory( ).setHelmet( ItemUtils.createItemStack( Material.IRON_HELMET, language.get( "items.pirates.iron_helm" ) ) );
                user.getPlayer( ).getInventory( ).setChestplate( ItemUtils.createItemStack( Material.GOLD_CHESTPLATE, language.get( "items.pirates.gold_chestplate" ) ) );
                user.getPlayer( ).getInventory( ).setLeggings( ItemUtils.createItemStack( Material.IRON_LEGGINGS, language.get( "items.pirates.iron_leggings" ) ) );
                user.getPlayer( ).getInventory( ).setBoots( ItemUtils.createItemStack( Material.GOLD_BOOTS, language.get( "items.pirates.gold_boots" ) ) );
            }

            if ( isPercentageBetween( 80, 99, false, true ) ) {

                user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.IRON_SWORD, language.get( "items.pirates.saber" ) ) );
                user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.EGG, 10, language.get( "items.pirates.grenade" ) ) );
                user.getPlayer( ).getInventory( ).setHelmet( ItemUtils.createItemStack( Material.GOLD_HELMET, language.get( "items.pirates.gold_helm" ) ) );
                user.getPlayer( ).getInventory( ).setChestplate( ItemUtils.createItemStack( Material.GOLD_CHESTPLATE, language.get( "items.pirates.gold_chestplate" ) ) );
                user.getPlayer( ).getInventory( ).setLeggings( ItemUtils.createItemStack( Material.GOLD_LEGGINGS, language.get( "items.pirates.gold_leggings" ) ) );
                user.getPlayer( ).getInventory( ).setBoots( ItemUtils.createItemStack( Material.GOLD_BOOTS, language.get( "items.pirates.gold_boots" ) ) );

            }

            if ( isPercentageBetween( 100, 101, false, true ) ) {

                user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.IRON_SPADE, language.get( "items.pirates.colt.big" ) ) );
                user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.EGG, 10, language.get( "items.pirates.grenade" ) ) );
                user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.GOLD_SWORD, language.get( "items.pirates.gold_sword" ) ) );
                user.getPlayer( ).getInventory( ).setHelmet( ItemUtils.createItemStack( Material.GOLD_HELMET, language.get( "items.pirates.gold_helm" ) ) );
                user.getPlayer( ).getInventory( ).setChestplate( ItemUtils.createItemStack( Material.GOLD_CHESTPLATE, language.get( "items.pirates.gold_chestplate" ) ) );
                user.getPlayer( ).getInventory( ).setLeggings( ItemUtils.createItemStack( Material.GOLD_LEGGINGS, language.get( "items.pirates.gold_leggings" ) ) );
                user.getPlayer( ).getInventory( ).setBoots( ItemUtils.createItemStack( Material.GOLD_BOOTS, language.get( "items.pirates.gold_boots" ) ) );
            }
        }

        InvUtil.Update( player );
    }

    public static void giveRandomKey ( ) {

        GameTeam gameTeam = null;

        for ( Map.Entry< Integer, GameTeam > entry : SWGameHandler.getInstance( ).getTeamModule( ).getTeams( ).entrySet( ) ) {
            if ( entry.getValue( ).getIndex( ) == 2 ) {
                gameTeam = entry.getValue( );
                break;
            }
        }

        List< Player > players = new ArrayList<>( );

        for ( Player player : gameTeam.getPlayerList( ) ) {
            players.add( player );
        }

        Random random = new Random( );
        int randomInt = random.nextInt( players.size( ) );

        User user = GameAPI.getUser( players.get( randomInt ) );
        Language language = user.getLanguage( );
        user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.TRIPWIRE_HOOK, language.get( "items.pirates.key" ) ) );
        InvUtil.Update( user.getPlayer( ) );

        SWGameHandler.getInstance( ).setPirateUserWithKey( user );

        user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "items.pirates.key.received" );

        for ( Player player : players ) {

            GameAPI.getUser( player ).sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "items.pirates.key.player.received", user.getColoredName( ) );
        }
    }

    public static boolean containsItem ( Player player, Material material ) {

        for ( ItemStack itemStack : player.getInventory( ).getContents( ) ) {

            if ( itemStack == null ) {
                continue;
            }

            if ( itemStack.getType( ) == material ) {

                return true;
            }
        }

        return false;
    }

    private static ItemStack createLeatherArmorItem ( Material material, Color color, Enchantment enchantment, Player player, String identifier ) {

        User user = GameAPI.getUser( player );
        Language language = user.getLanguage( );

        ItemStack itemStack = ItemUtils.createItemStack( material, language.get( identifier ) );
        LeatherArmorMeta meta = ( LeatherArmorMeta ) itemStack.getItemMeta( );
        meta.setColor( color );
        if ( enchantment != null ) itemStack.addEnchantment( enchantment, 1 );
        itemStack.setItemMeta( meta );

        return itemStack;
    }

    public static boolean isPercentageBetween ( int d1, int d2, boolean time, boolean gold ) {

        String percentage = "";

        if ( time == false && gold == true ) {

            percentage = NumberFormat.getPercentInstance( ).format( SWArenaHandler.getInstance( ).getSelectedGold( ) / SWArenaHandler.getInstance( ).getGold( ) );
        }

        if ( time == true && gold == false ) {

            Countdown countdown = SWGameHandler.getInstance( ).getCurrentPhase( ).getCountdown( );
            double startValue = countdown.getStartValue( );
            double currentValue = countdown.getCurrentValue( );

            double difference = startValue - currentValue;

            percentage = NumberFormat.getPercentInstance( ).format( difference / startValue );
        }

        for ( int i = d1; i < d2; i++ ) {

            if ( percentage.equals( i + "%" ) ) {

                return true;
            }
        }

        return false;
    }
}
