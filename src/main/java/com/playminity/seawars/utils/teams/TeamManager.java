package com.playminity.seawars.utils.teams;

import com.playminity.seawars.Data;
import com.playminity.seawars.SWGameHandler;
import com.playminity.seawars.SeaWars;
import lombok.Getter;
import me.kombustorlp.gameapi.misc.User;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by André on 02.11.2016.
 */
public class TeamManager {

    @Getter
    private static SWGameHandler swGameHandler = SWGameHandler.getInstance( );

    @Getter
    private static Map< User, CompassType > userCompassTypeMap = new HashMap<>( );

    public static void onTeamBlancing ( ) {

        for ( Player player : Bukkit.getOnlinePlayers( ) ) {

            if ( SWGameHandler.getInstance( ).getTeamModule( ).getTeam( player ) == null ) {

                if ( swGameHandler.getTeam( 1 ).getPlayerList( ).size( ) == swGameHandler.getTeam( 2 ).getPlayerList( ).size( ) ) {

                    swGameHandler.getTeam( 2 ).join( player );
                    continue;
                }

                if ( swGameHandler.getTeam( 1 ).getPlayerList( ).size( ) > swGameHandler.getTeam( 2 ).getPlayerList( ).size( ) ) {

                    swGameHandler.getTeam( 2 ).join( player );
                    continue;
                }

                if ( swGameHandler.getTeam( 1 ).getPlayerList( ).size( ) < swGameHandler.getTeam( 2 ).getPlayerList( ).size( ) ) {

                    swGameHandler.getTeam( 1 ).join( player );
                    continue;
                }
            }
        }

        int onlinePlayerCount = Bukkit.getOnlinePlayers( ).size( );
        int amountOfPirates = swGameHandler.getTeam( 2 ).getPlayerList( ).size( );
        int amountOfSailors = swGameHandler.getTeam( 1 ).getPlayerList( ).size( );

        if ( amountOfPirates + amountOfSailors == onlinePlayerCount ) {

            if ( onlinePlayerCount % 2 == 0 ) {

                while ( getDifference( ) >= 1 ) {

                    onTeamBlancing( 2 );
                }

                while ( getDifference( ) <= - 1 ) {

                    onTeamBlancing( 1 );
                }

                return;
            }

            while ( getDifference( ) > 1 ) {

                onTeamBlancing( 2 );
            }

            while ( getDifference( ) < - 1 ) {

                onTeamBlancing( 1 );
            }

            return;
        }

    }

    private static int getDifference ( ) {

        return ( swGameHandler.getTeam( 1 ).getPlayerList( ).size( ) - swGameHandler.getTeam( 2 ).getPlayerList( ).size( ) );
    }

    private static void onTeamBlancing ( int index ) {

        if ( index == 1 ) {
            List< Player > players = new ArrayList<>( );

            swGameHandler.getTeam( 2 ).getPlayerList( ).forEach( player -> {
                players.add( player );
            } );
            swGameHandler.getTeam( 2 ).leave( players.get( players.size( ) - 1 ) );
            swGameHandler.getTeam( 1 ).join( players.get( players.size( ) - 1 ) );
            players.clear( );

            return;
        }

        List< Player > players = new ArrayList<>( );

        swGameHandler.getTeam( 1 ).getPlayerList( ).forEach( player -> {
            players.add( player );
        } );
        swGameHandler.getTeam( 1 ).leave( players.get( players.size( ) - 1 ) );
        swGameHandler.getTeam( 2 ).join( players.get( players.size( ) - 1 ) );
        players.clear( );
    }

    public static void startCommpassSchedule ( ) {

        new BukkitRunnable( ) {

            @Override
            public void run ( ) {

                if ( userCompassTypeMap.size( ) == 0 ) {

                    return;
                }

                for ( Map.Entry< User, CompassType > userCompassTypeEntry : userCompassTypeMap.entrySet( ) ) {

                    if ( userCompassTypeEntry.getValue( ) == CompassType.HOSTAGE ) {

                        userCompassTypeEntry.getKey( ).getPlayer( ).setCompassTarget( Data.getVotedArena( ).getData( ).getSpawn( "hostage", 1 ).getLocation( ) );
                    }

                    if ( userCompassTypeEntry.getValue( ) == CompassType.KEY ) {

                        if ( SWGameHandler.getInstance( ).getPirateUserWithKey( ) == null ) {

                            userCompassTypeEntry.getKey( ).getPlayer( ).setCompassTarget( Data.getVotedArena( ).getData( ).getSpawn( "hostage", 1 ).getLocation( ) );
                            return;
                        }

                        userCompassTypeEntry.getKey( ).getPlayer( ).setCompassTarget( SWGameHandler.getInstance( ).getPirateUserWithKey( ).getPlayer( ).getLocation( ) );
                    }
                }

            }
        }.runTaskTimer( SeaWars.getInstance( ), 20, 20 );
    }

    public enum CompassType {

        HOSTAGE,
        KEY;
    }
}
