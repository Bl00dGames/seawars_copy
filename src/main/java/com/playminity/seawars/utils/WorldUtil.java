package com.playminity.seawars.utils;

import com.playminity.seawars.Data;
import com.playminity.seawars.SWGameHandler;
import com.playminity.seawars.arenasystem.SWArenaHandler;
import me.kombustorlp.gameapi.GameAPI;
import me.kombustorlp.gameapi.utils.FakeNPC;
import me.kombustorlp.gameapi.utils.ProtocolAPI;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.EntityType;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by André on 01.11.2016.
 */
public class WorldUtil {

    public static Map< Location, Block > getBlocksOfMaterial ( Location location, int radius, Material material ) {

        Map< Location, Block > blockHashMap = new HashMap<>( );

        int radiusSquared = radius * radius;
        World world = location.getWorld( );
        for ( int x = - radius; x <= radius; x++ ) {
            for ( int y = - radius; y <= radius; y++ ) {
                for ( int z = - radius; z <= radius; z++ ) {
                    if ( ( x * x ) + ( z * z ) <= radiusSquared ) {
                        if ( world.getBlockAt( location.getBlockX( ) + x, location.getBlockY( ) + y, location.getBlockZ( ) + z ).getType( ) == material ) {
                            Location location1 = new Location( location.getWorld( ), location.getBlockX( ) + x, location.getBlockY( ) + y, location.getBlockZ( ) + z );
                            blockHashMap.put( location1, world.getBlockAt( location.getBlockX( ) + x, location.getBlockY( ) + y, location.getBlockZ( ) + z ) );
                        }
                    }
                }
            }
        }

        return blockHashMap;
    }

    public static void getBlocksOfMaterial ( Location location, int radius, Material material, HashMap< Location, Block > hashMap ) {

        int radiusSquared = radius * radius;
        World world = location.getWorld( );
        for ( int x = - radius; x <= radius; x++ ) {
            for ( int y = - radius; y <= radius; y++ ) {
                for ( int z = - radius; z <= radius; z++ ) {
                    if ( ( x * x ) + ( z * z ) <= radiusSquared ) {
                        if ( world.getBlockAt( location.getBlockX( ) + x, location.getBlockY( ) + y, location.getBlockZ( ) + z ).getType( ) == material ) {
                            Location location1 = new Location( location.getWorld( ), location.getBlockX( ) + x, location.getBlockY( ) + y, location.getBlockZ( ) + z );
                            hashMap.put( location1, world.getBlockAt( location.getBlockX( ) + x, location.getBlockY( ) + y, location.getBlockZ( ) + z ) );
                        }
                    }
                }
            }
        }
    }

    public static void spawnPirateVillager ( ) {

        /** Pirate Villager */

        CraftEntity craftEntity = ( ( CraftEntity ) Data.getVotedArena( ).getData( ).getWorld( ).spawnEntity( Data.getVotedArena( ).getData( ).getSpawn( "ptrader", 1 ).getLocation( ), EntityType.VILLAGER ) );
        craftEntity.setCustomName( "§cMarc" );
        craftEntity.setCustomNameVisible( true );
        net.minecraft.server.v1_8_R3.Entity craftEntityHandle = craftEntity.getHandle( );

        NBTTagCompound nbtTagCompound = new NBTTagCompound( );
        craftEntityHandle.e( nbtTagCompound );
        if ( nbtTagCompound == null )
            nbtTagCompound = new NBTTagCompound( );
        craftEntityHandle.c( nbtTagCompound );
        nbtTagCompound.setInt( "NoAI", 1 );
        craftEntityHandle.f( nbtTagCompound );

        SWArenaHandler.getInstance( ).setCraftEntity( craftEntity );
        SWGameHandler.getInstance( ).getTeam( 2 ).getPlayerList( ).forEach( player -> {

            ProtocolAPI.sendActionbar( player, GameAPI.getUser( player ).getLanguage( ).get( "villager.pirates.trade.spawn" ) );
            GameAPI.getUser( player ).sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "villager.pirates.trade.spawn" );
        } );
    }

    public static void despawnPirateVillager ( ) {

        SWArenaHandler.getInstance( ).getCraftEntity( ).remove( );
        SWArenaHandler.getInstance( ).setCraftEntity( null );
        SWGameHandler.getInstance( ).getTeam( 2 ).getPlayerList( ).forEach( player -> {
            ProtocolAPI.sendActionbar( player, GameAPI.getUser( player ).getLanguage( ).get( "villager.pirates.trade.despawn" ) );
            GameAPI.getUser( player ).sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "villager.pirates.trade.despawn" );
        } );
    }

    public static void setupMap ( ) {

        /** Gold Villager */

        for ( int i = 1; i <= 3; i++ ) {

            Location location2 = Data.getVotedArena( ).getData( ).getSpawn( "villager", i ).getLocation( );

            CraftEntity craftEntity2 = ( ( CraftEntity ) location2.getWorld( ).spawnEntity( location2, EntityType.VILLAGER ) );
            craftEntity2.setCustomName( "§eGold" );
            craftEntity2.setCustomNameVisible( true );
            net.minecraft.server.v1_8_R3.Entity craftEntityHandle2 = craftEntity2.getHandle( );

            NBTTagCompound nbtTagCompound2 = new NBTTagCompound( );
            craftEntityHandle2.e( nbtTagCompound2 );
            if ( nbtTagCompound2 == null )
                nbtTagCompound2 = new NBTTagCompound( );
            craftEntityHandle2.c( nbtTagCompound2 );
            nbtTagCompound2.setInt( "NoAI", 1 );
            craftEntityHandle2.f( nbtTagCompound2 );
        }

        SWGameHandler.getInstance( ).setVillager( 3 );

        /** Hostage */

        Location location3 = Data.getVotedArena( ).getData( ).getSpawn( "hostage", 1 ).getLocation( );

        SWGameHandler.getInstance( ).setFakeNPC( new FakeNPC( "Maike <3", location3 ) );

        SWGameHandler.getInstance( ).getFakeNPC( ).spawn( );

        SWGameHandler.getInstance( ).setHostage( true );
    }

    public static double calculatePercent ( double goldCollected ) {

        String percentage = NumberFormat.getPercentInstance( ).format( goldCollected / SWArenaHandler.getInstance( ).getGold( ) );
        percentage = percentage.replace( "%", "" );

        return Double.valueOf( percentage );
    }
}
