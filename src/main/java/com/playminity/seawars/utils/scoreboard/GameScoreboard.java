package com.playminity.seawars.utils.scoreboard;

import com.google.common.collect.Lists;
import com.playminity.minigameframework.MGFramework;
import com.playminity.minigameframework.scoreboard.SimpleGameBoard;
import com.playminity.minigameframework.scoreboard.utils.ScoreboardLine;
import com.playminity.seawars.SWGameHandler;
import com.playminity.seawars.arenasystem.SWArenaHandler;
import me.kombustorlp.gameapi.languagesystem.Language;

import java.text.NumberFormat;
import java.util.List;

/**
 * Created by André on 31.10.2016.
 */
public class GameScoreboard extends SimpleGameBoard {

    public GameScoreboard ( ) {

        super( "gameboard", - 1 );
    }

    @Override
    public String getTitle ( Language language ) {

        return "§8» §6§l" + MGFramework.getGame( ).getDisplayName( ) + " §8«";
    }

    @Override
    public ScoreboardLine[] getLines ( Language language ) {

        List< ScoreboardLine > lines = Lists.newArrayList( );

        lines.add( new ScoreboardLine( " " ) );
        lines.add( new ScoreboardLine( language.get( "scoreboard.line.1", NumberFormat.getPercentInstance( ).format( SWArenaHandler.getInstance( ).getSelectedGold( ) / SWArenaHandler.getInstance( ).getGold( ) ) ) ) );
        lines.add( new ScoreboardLine( "  " ) );
        lines.add( new ScoreboardLine( language.get( "scoreboard.line.2", SWGameHandler.getInstance( ).getVillager( ) ) ) );
        lines.add( new ScoreboardLine( language.get( "scoreboard.line.3", ( SWGameHandler.getInstance( ).isHostage( ) == true ? "Nein" : "Ja" ) ) ) );
        lines.add( new ScoreboardLine( "   " ) );
        lines.add( new ScoreboardLine( language.get( "scoreboard.line.5", ( SWGameHandler.getInstance( ).getTeam( 2 ) == null ? 0 : SWGameHandler.getInstance( ).getTeam( 2 ).getPlayerList( ).size( ) ) ) ) );
        lines.add( new ScoreboardLine( language.get( "scoreboard.line.6", ( SWGameHandler.getInstance( ).getTeam( 1 ) == null ? 0 : SWGameHandler.getInstance( ).getTeam( 1 ).getPlayerList( ).size( ) ) ) ) );
        return lines.toArray( new ScoreboardLine[ lines.size( ) ] );
    }
}
