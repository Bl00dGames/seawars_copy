package com.playminity.seawars.arenasystem;

import com.playminity.rest.internalapi.routes.utils.Game;
import lombok.Getter;
import me.kombustorlp.gameapi.arenamanager.Arena;

/**
 * Created by Andr� on 03.10.2016.
 */
public class SWCreateTArena {

    @Getter
    private final SWTArenaData data;

    public SWCreateTArena ( ) {

        this.data = new SWTArenaData( );
    }

    public void store ( ) {

        Arena< SWTArenaData > arena = new Arena<>( Game.SEAWARS.getName( ), "Tutorial", data, SWTArenaData.class );

        SWArenaHandler.getInstance( ).storeTArena( arena );
    }
}
