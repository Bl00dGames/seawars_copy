package com.playminity.seawars.arenasystem;

import lombok.Getter;
import lombok.Setter;
import me.kombustorlp.gameapi.arenamanager.IArenaData;
import me.kombustorlp.gameapi.arenamanager.LocationWrapper;
import me.kombustorlp.gameapi.utils.WorldManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by André on 11.11.2016.
 */
public class SWTArenaData extends IArenaData {

    @Getter
    @Setter
    private String worldName;
    @Getter
    @Setter
    private Map< Integer, LocationWrapper > spawns;
    @Getter
    @Setter
    private Map< Integer, String > messages;
    private transient boolean copied = false;

    public SWTArenaData ( ) {

        spawns = new HashMap<>( );
        messages = new HashMap<>( );
    }

    public void addSpawn ( int index, Location location, String messageIndex ) {

        worldName = location.getWorld( ).getName( );
        if ( spawns.get( index ) != null ) spawns.remove( index );
        spawns.put( index, new LocationWrapper( location ) );
        messages.put( index, messageIndex );
    }

    public LocationWrapper getSpawn ( int id ) {

        return spawns.get( id );
    }

    public String getMessageIndex ( int id ) {

        return messages.get( id );
    }

    public World getWorld ( ) {

        return Bukkit.getWorld( worldName );
    }

    public void copyWorld ( ) throws Exception {

        if ( copied )
            return;
        WorldManager.prepareWorld( worldName );
        copied = true;
    }

    public boolean loadWorld ( ) {

        return WorldManager.loadWorld( worldName );
    }
}
