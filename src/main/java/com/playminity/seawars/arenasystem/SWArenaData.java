package com.playminity.seawars.arenasystem;

import lombok.Getter;
import lombok.Setter;
import me.kombustorlp.gameapi.arenamanager.IArenaData;
import me.kombustorlp.gameapi.arenamanager.LocationWrapper;
import me.kombustorlp.gameapi.utils.WorldManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Andr� on 03.10.2016.
 */
public class SWArenaData extends IArenaData {

    @Getter
    @Setter
    private String worldName;
    @Getter
    @Setter
    private LocationWrapper spectatorSpawn;
    @Getter
    @Setter
    private Map< String, LocationWrapper > spawns;
    private transient boolean copied = false;

    public SWArenaData ( ) {

        spawns = new HashMap<>( );
    }

    public void addSpawn ( String index, Location location ) {

        worldName = location.getWorld( ).getName( );
        if ( spawns.get( index ) != null ) spawns.remove( index );
        spawns.put( index, new LocationWrapper( location ) );
    }

    public LocationWrapper getSpawn ( String index, int id ) {

        return spawns.get( index + "." + id );
    }

    public World getWorld ( ) {

        return Bukkit.getWorld( worldName );
    }

    public void copyWorld ( ) throws Exception {

        if ( copied )
            return;
        WorldManager.prepareWorld( worldName );
        copied = true;
    }

    public boolean loadWorld ( ) {

        return WorldManager.loadWorld( worldName );
    }
}
