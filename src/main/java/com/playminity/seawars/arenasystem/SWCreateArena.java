package com.playminity.seawars.arenasystem;

import com.playminity.rest.internalapi.routes.utils.Game;
import lombok.Getter;
import me.kombustorlp.gameapi.arenamanager.Arena;

/**
 * Created by Andr� on 03.10.2016.
 */
public class SWCreateArena {

    @Getter
    private final String name;
    @Getter
    private final SWArenaData data;

    public SWCreateArena ( String name ) {

        this.name = name;
        this.data = new SWArenaData( );
    }

    public void store ( ) {

        Arena< SWArenaData > arena = new Arena<>( Game.SEAWARS.getName( ), name, data, SWArenaData.class );

        SWArenaHandler.getInstance( ).storeArena( arena );
    }
}
