package com.playminity.seawars.arenasystem;

import com.playminity.rest.internalapi.routes.utils.Game;
import lombok.Getter;
import lombok.Setter;
import me.kombustorlp.gameapi.arenamanager.Arena;
import me.kombustorlp.gameapi.arenamanager.ArenaHandler;
import me.kombustorlp.gameapi.exceptions.NotEnoughArenasException;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Andr� on 03.10.2016.
 */
public class SWArenaHandler {

    @Setter
    @Getter
    private static SWArenaHandler instance;
    @Getter
    ArenaHandler< SWArenaData > arenaHandler;
    @Getter
    ArenaHandler< SWTArenaData > tarenaHandler;
    @Getter
    @Setter
    private Map< Location, Block > blockList = new HashMap<>( );
    @Getter
    @Setter
    private double gold;
    @Getter
    @Setter
    private double selectedGold = 0;
    @Getter
    @Setter
    private CraftEntity craftEntity = null;

    public SWArenaHandler ( ) {

        arenaHandler = new ArenaHandler<>( Game.SEAWARS.getName( ), SWArenaData.class );
        tarenaHandler = new ArenaHandler<>( Game.SEAWARS.getName( ), SWTArenaData.class );
    }

    public List< Arena< SWArenaData > > loadArenas ( ) throws NotEnoughArenasException {

        try {

            arenaHandler.load( Game.SEAWARS, 3 );
        } catch ( Exception exception ) {

            System.err.println( "[SeaWars] Could not load arenas." );
        }

        if ( arenaHandler.getArenas( ).size( ) != 3 )
            throw new NotEnoughArenasException( "Not enough arenas found (found " + arenaHandler.getArenas( ).size( ) + ")" );
        return arenaHandler.getArenas( );
    }

    public List< Arena< SWTArenaData > > loadTutorialArenas ( ) throws NotEnoughArenasException {

        try {

            tarenaHandler.load( Game.SEAWARS, 1 );
        } catch ( Exception exception ) {

            System.err.println( "[SeaWars] Could not load tutorial_arenas." );
        }

        if ( tarenaHandler.getArenas( ).size( ) != 1 )
            throw new NotEnoughArenasException( "Not enough tutorial_arenas found (found " + tarenaHandler.getArenas( ).size( ) + ")" );
        return tarenaHandler.getArenas( );
    }

    public void storeArena ( Arena< SWArenaData > arena ) {

        arenaHandler.store( arena.getName( ), Bukkit.getMaxPlayers( ), arena.getData( ) );
    }

    public void storeTArena ( Arena< SWTArenaData > arena ) {

        tarenaHandler.store( arena.getName( ), Bukkit.getMaxPlayers( ), arena.getData( ) );
    }
}
