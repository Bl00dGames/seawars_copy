package com.playminity.seawars;

import com.playminity.minigameframework.handler.GameHandler;
import com.playminity.minigameframework.handler.phase.GamePhase;
import com.playminity.minigameframework.modules.utils.GameTeam;
import com.playminity.seawars.arenasystem.SWArenaHandler;
import com.playminity.seawars.utils.SWAchievements;
import com.playminity.seawars.utils.SWStats;
import com.playminity.seawars.utils.WorldUtil;
import lombok.Getter;
import lombok.Setter;
import me.kombustorlp.gameapi.GameAPI;
import me.kombustorlp.gameapi.minityshop.LoadoutInventory;
import me.kombustorlp.gameapi.misc.User;
import me.kombustorlp.gameapi.utils.FakeNPC;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.Map;
import java.util.Random;

/**
 * Created by Andr� on 03.10.2016.
 */
public class SWGameHandler extends GameHandler {

    @Getter
    @Setter
    private static SWGameHandler instance;

    @Getter
    @Setter
    private int villager;

    @Getter
    @Setter
    private boolean hostage;

    @Getter
    @Setter
    private FakeNPC fakeNPC;

    @Getter
    @Setter
    private boolean sailorsUpgrade = false;

    @Getter
    @Setter
    private int latestUpgrade = 0;

    @Getter
    @Setter
    private int piratesKills = 0;

    @Getter
    @Setter
    private User pirateUserWithKey;

    public SWGameHandler ( GamePhase phaseOnEnd ) {

        this( phaseOnEnd, null );
    }

    public SWGameHandler ( GamePhase phaseOnEnd, LoadoutInventory loadout ) {

        super( "§7❱ §e§lSeaWars §7» §r", 1000000, Data.getConfig( ).getTeamSize( ), false, Data.getConfig( ).getMinPlayers( ), Data.getConfig( ).getJoinPrivilege( ), phaseOnEnd, false, loadout );
        setPointStat( SWStats.POINTS );
        setStatsModule( SeaWars.getStatsModule( ) );
        setIgnoreTeamLives( true );
    }

    @Override
    public Player[] getScoreboardPlayers ( GamePhase gamePhase ) {

        return new Player[ 0 ];
    }

    @Override
    public Location getSpectatorSpawn ( ) {

        return null;
    }

    @Override
    public Location getSpawn ( Player player ) {

        GameTeam gameTeam = SWGameHandler.getInstance( ).getTeamModule( ).getTeam( player );

        if ( gameTeam == null ) {

            return Data.getVotedArena( ).getData( ).getSpectatorSpawn( ).getLocation( );
        }

        if ( gameTeam.getIndex( ) == 1 ) {

            if ( WorldUtil.calculatePercent( SWArenaHandler.getInstance( ).getSelectedGold( ) ) == 100 ) {

                return Data.getVotedArena( ).getData( ).getSpectatorSpawn( ).getLocation( );
            }

            return getRandomLocation( "sailors" );
        }

        if ( gameTeam.getIndex( ) == 2 ) {

            if ( isHostage( ) == false ) {

                return Data.getVotedArena( ).getData( ).getSpectatorSpawn( ).getLocation( );
            }

            return getRandomLocation( "pirates" );
        }

        return null;
    }

    @Override
    public void onDeath ( Player player, User user, Player playerKiller ) {

        if ( playerKiller != null && playerKiller.isOnline( ) ) {

            SeaWars.getStatsModule( ).add( GameAPI.getUser( playerKiller ), SWStats.KILLS, 1 );
            GameAPI.getUser( playerKiller ).callAchievement( SWAchievements.FIRSTKILL );
        }

        SeaWars.getStatsModule( ).add( user, SWStats.DEATHS, 1 );
    }

    @Override
    public void onOutOfGameDeath ( Player player, User user ) {

    }

    @Override
    public void onRespawn ( Player player, User user ) {

    }

    @Override
    public void onWin ( GameTeam gameTeam ) {

        gameTeam.getPlayerList( ).forEach( player -> {

            SeaWars.getStatsModule( ).add( GameAPI.getUser( player ), SWStats.WINS, 1 );
            GameAPI.getUser( player ).callAchievement( SWAchievements.FIRSTWIN );
        } );
    }

    @Override
    public void onNoWinner ( ) {

    }

    public GameTeam getTeam ( int id ) {

        for ( Map.Entry< Integer, GameTeam > entry : getTeamModule( ).getTeams( ).entrySet( ) ) {

            if ( entry.getValue( ).getIndex( ) == id ) {

                return entry.getValue( );
            }
        }

        return null;
    }

    private Location getRandomLocation ( String index ) {

        Random random = new Random( );
        int nextInt = random.nextInt( 5 );

        while ( nextInt == 0 ) {

            nextInt = random.nextInt( 5 );
        }

        return Data.getVotedArena( ).getData( ).getSpawn( index, nextInt ).getLocation( );
    }
}
