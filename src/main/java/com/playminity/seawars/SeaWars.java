package com.playminity.seawars;

import com.playminity.minigameframework.MGData;
import com.playminity.minigameframework.MGFramework;
import com.playminity.minigameframework.commands.ForceStartCommand;
import com.playminity.minigameframework.commands.StatsCommand;
import com.playminity.minigameframework.handler.phase.GamePhase;
import com.playminity.minigameframework.handler.utils.DefaultVoteScoreboard;
import com.playminity.minigameframework.modules.*;
import com.playminity.minigameframework.stats.PlayerGameStats;
import com.playminity.rest.internalapi.routes.utils.Game;
import com.playminity.seawars.arenasystem.SWArenaData;
import com.playminity.seawars.arenasystem.SWArenaHandler;
import com.playminity.seawars.arenasystem.SWTArenaData;
import com.playminity.seawars.commands.SWCommand;
import com.playminity.seawars.gamephase.*;
import com.playminity.seawars.listener.*;
import com.playminity.seawars.minityshop.KillEffects;
import com.playminity.seawars.minityshop.ShopCategories;
import com.playminity.seawars.utils.SWAchievements;
import com.playminity.seawars.utils.SWConfig;
import com.playminity.seawars.utils.SWStats;
import com.playminity.seawars.utils.TutorialManager;
import lombok.Getter;
import me.kombustorlp.gameapi.GameAPI;
import me.kombustorlp.gameapi.achievements.AchievementManager;
import me.kombustorlp.gameapi.arenamanager.Arena;
import me.kombustorlp.gameapi.exceptions.NotEnoughArenasException;
import me.kombustorlp.gameapi.languagesystem.Language;
import me.kombustorlp.gameapi.libraries.material.MaterialInfo;
import me.kombustorlp.gameapi.minityshop.LoadoutInventory;
import me.kombustorlp.gameapi.minityshop.MinityShop;
import me.kombustorlp.gameapi.misc.Option;
import me.kombustorlp.gameapi.misc.Rang;
import me.kombustorlp.gameapi.servermanager.ServerManager;
import me.kombustorlp.gameapi.utils.WorldManager;
import me.kombustorlp.gameapi.utils.WorldUtils;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Andr� on 28.09.2016.
 */
public class SeaWars extends JavaPlugin {

    @Getter
    private static SeaWars instance;
    @Getter
    private static StatsModule statsModule;
    @Getter
    public LoadoutInventory loadout;
    @Getter
    private GamePhase lobbyPhase, beginPhase, fightPhase, endPhase, tutorialPhase;
    @Getter
    private VoteModule voteModule;
    @Getter
    private ChatModule chatModule;
    @Getter
    private TutorialManager tutorialManager;

    @Override
    public void onEnable ( ) {

        instance = this;

        tutorialManager = new TutorialManager( this );

        SWArenaHandler.setInstance( new SWArenaHandler( ) );

        GameAPI.setAntiCheatEnabled( false );
        GameAPI.setGameType( Game.SEAWARS.getName( ) );

        GameAPI.setOptions( new HashMap< Option, Boolean >( ) {

            private static final long serialVersionUID = 1L;

            {
                put( Option.JOINMESSAGES, false );
                put( Option.FORMATCHAT, true );
                put( Option.NICKSUSABLE, true );
                put( Option.BLOCK_FIRESPREAD, true );
                put( Option.BLOCK_LEAVEDECAY, true );
                put( Option.BLOCK_ICEMELT, true );
                put( Option.BLOCK_HANGINGBREAK, true );
                put( Option.BLOCK_ITEMFRAMEINTERACT, true );
                put( Option.BLOCK_RAIN, true );
                put( Option.ISHUBSERVER, false );
            }
        } );

        getCommand( "seawars" ).setExecutor( new SWCommand( ) );

        PluginManager pluginManager = Bukkit.getPluginManager( );
        pluginManager.registerEvents( new MiscListener( ), this );
        pluginManager.registerEvents( new VillagerListener( ), this );
        pluginManager.registerEvents( new PlayerManageListener( ), this );
        pluginManager.registerEvents( new GoldCollectListener( ), this );
        pluginManager.registerEvents( new HostageListener( ), this );
        pluginManager.registerEvents( new ItemListener( ), this );

        loadMessages( );
        loadFramework( );

        AchievementManager.setup( "SeaWars", new MaterialInfo( Material.CHEST ), SWAchievements.values( ) );
        MinityShop.newRegistration( "SeaWars" ).registerCategory( ShopCategories.values( ) ).registerArticles( KillEffects.values( ) ).finish( );

        ServerManager.openServer( "Voting..." );

    }

    private void loadFramework ( ) {

        MGFramework.setDebug( true );
        MGFramework.initialize( Game.SEAWARS );

        prepareConfig( );

        endPhase = new EndPhase( );
        SWGameHandler gameHandler = new SWGameHandler( endPhase, loadout = new LoadoutInventory( "SeaWars" ) );
        SWGameHandler.setInstance( gameHandler );
        gameHandler.addGamePhase( lobbyPhase = new LobbyPhase( ) );
        gameHandler.addGamePhase( tutorialPhase = new TutorialPhase( ) );
        gameHandler.addGamePhase( beginPhase = new BeginPhase( ) );
        gameHandler.addGamePhase( fightPhase = new FightPhase( ) );
        gameHandler.addGamePhase( endPhase );

        List< Arena< SWArenaData > > arenas = null;

        try {

            arenas = SWArenaHandler
                    .getInstance( )
                    .loadArenas( );
            Data.setTutorialArena( SWArenaHandler.getInstance( ).loadTutorialArenas( ).get( 0 ) );
        } catch ( NotEnoughArenasException exception ) {

            System.out.println( "[SeaWars] Not enough arenas are loaded." );
        }

        for ( Arena< SWArenaData > arena : arenas ) {

            if ( arena == null || arena.getData( ) == null || arena.getData( ).getWorld( ) == null ) continue;

            if ( ! WorldManager.isWorld( arena.getData( ).getWorld( ).getName( ) ) ) {

                WorldManager.loadWorld( arena.getData( ).getWorld( ).getName( ) );
                continue;
            }

            for ( Entity entity : arena.getData( ).getWorld( ).getEntities( ) ) {
                entity.remove( );
            }
        }

        if ( Data.getTutorialArena( ) != null ) {

            Arena< SWTArenaData > arena = Data.getTutorialArena( );

            try {

                arena.getData( ).copyWorld( );
            } catch ( Exception exception ) {
                exception.printStackTrace( );
            }

            arena.getData( ).loadWorld( );
            arena.getData( ).getWorld( ).setGameRuleValue( "keepInventory", "false" );
            arena.getData( ).getWorld( ).setGameRuleValue( "doDaylightCycle", "true" );
            arena.getData( ).getWorld( ).setDifficulty( Difficulty.EASY );
            arena.getData( ).getWorld( ).setTime( 0L );

            WorldUtils.setDisableWorldTickWithoutPlayers( arena.getData( ).getWorld( ), true );
            WorldUtils.setDisableBlockTicks( arena.getData( ).getWorld( ), false );
        }

        voteModule = new VoteModule< SWArenaData >( arenas.toArray( new Arena[ arenas.size( ) ] ) ) {

            @Override
            public void loadMap ( Arena< SWArenaData > arena ) {

                Data.setVotedArena( arena );

                Bukkit.getScheduler( ).runTaskAsynchronously( SeaWars.getInstance( ), ( ) -> {

                    try {

                        arena.getData( ).copyWorld( );
                    } catch ( Exception exception ) {

                        exception.printStackTrace( );
                    }

                    Bukkit.getScheduler( ).runTask( SeaWars.getInstance( ), ( ) -> {

                        arena.getData( ).loadWorld( );
                        arena.getData( ).getWorld( ).setGameRuleValue( "keepInventory", "false" );
                        arena.getData( ).getWorld( ).setGameRuleValue( "doDaylightCycle", "true" );
                        arena.getData( ).getWorld( ).setDifficulty( Difficulty.EASY );
                        arena.getData( ).getWorld( ).setTime( 0L );

                        WorldUtils.setDisableWorldTickWithoutPlayers( arena.getData( ).getWorld( ), true );
                        WorldUtils.setDisableBlockTicks( arena.getData( ).getWorld( ), false );
                    } );
                } );

            }
        };

        DefaultVoteScoreboard< SWArenaData > voteScoreboard = new DefaultVoteScoreboard<>( voteModule );

        lobbyPhase.setScoreboard( voteScoreboard );
        voteModule.setBoard( voteScoreboard );
        MGFramework.loadModule( voteModule );

        MGFramework.loadLeaderboardWall( MGData.DEFAULT_LEADERBOARD_DESIGN );

        MGFramework.loadModule(
                statsModule = new StatsModule( SWStats.values( ), new StatsCommand( Game.SEAWARS ) {

                    @Override
                    public void modifyPlayerGameStats ( PlayerGameStats playerGameStats ) {

                        playerGameStats.addStatByDivision( "kd", "kills", "deaths" );
                        playerGameStats.addStat( "rank", playerGameStats.getWrapper( ).getRank( ) );
                        playerGameStats.setChatOrder( "rank", "points", "gamesplayed", "wins", "kills", "deaths", "kd", "gold", "pirates", "sailors" );
                    }
                } )
        );

        MGFramework.loadModule( new FoodModule( ) );
        MGFramework.loadModule( chatModule = new ChatModule( true, true, "§7[§4§lX§7] §r" ) );

        MGFramework.registerCommand( new ForceStartCommand( Rang.YOUTUBER, lobbyPhase, 11 ) );

    }

    private void prepareConfig ( ) {

        ConfigModule< SWConfig > configModule = new ConfigModule< SWConfig >( "setup-config" );
        try {

            Data.setConfig( configModule.load( new SWConfig( ), SWConfig.class ) );

        } catch ( IOException exception ) {

            System.out.println( "Failed to load config." );
            exception.printStackTrace( );
            Bukkit.shutdown( );
        }
    }

    private void loadMessages ( ) {

        /** German-messages */
        Language.DEUTSCH.add( "nopermissions", "§cDu hast keine Berechtigung für diesen Befehl." );

        Language.DEUTSCH.add( "sw.command.arena.help.1", "§e/sw arena create <Name> §8| §eErstelle eine Arena" );
        Language.DEUTSCH.add( "sw.command.arena.help.2", "§e/sw build §8| §eBerechtigung zum Umbau der Arena erhalten" );
        Language.DEUTSCH.add( "sw.command.arena.creating", "§cDu erstellst bereits eine Arena, beende dies mit /sw arena build." );
        Language.DEUTSCH.add( "sw.command.arena.create", "§eDu haste eine Arena erstellt: %s" );
        Language.DEUTSCH.add( "sw.command.arena.addspawn", "§eVerwende /sw arena addSpawn <Type> <ID> §8| §e#1" );
        Language.DEUTSCH.add( "sw.command.arena.setspecspawn", "§eVerwende /sw arena setspecspawn §8| §e#2" );
        Language.DEUTSCH.add( "sw.command.arena.build", "§eVerwende /sw arena build §8| §e#3" );
        Language.DEUTSCH.add( "sw.command.arena.build.success", "§eDu hast jetzt Rechte zum Bauen." );
        Language.DEUTSCH.add( "sw.command.arena.build.remove", "§4Du hast keine Rechte mehr zum Bauen." );
        Language.DEUTSCH.add( "sw.command.arena.addspawn", "§eDu hast einen Spawn vom Typ §c%s §emit der ID §c%s §ehinzugefügt" );
        Language.DEUTSCH.add( "sw.command.arena.notcreating", "§cDu erstellst derzeit keine Arena." );
        Language.DEUTSCH.add( "sw.command.arena.setspectatorspawn", "§eDu hast den Spectator-Spawn für §c%s §egesetzt." );
        Language.DEUTSCH.add( "sw.command.arena.build", "§eDu hast erfolgreich die Arena §c%s §egespeichert." );
        Language.DEUTSCH.add( "sw.command.arena.exists", "§cDie Arena exisitert bereits." );

        Language.DEUTSCH.add( "sw.command.tutorial.help.1", "§e/sw tutorial create §8| §eErstelle das Tutorial" );
        Language.DEUTSCH.add( "sw.command.tutorial.help.2", "§e/tutorial leave §8| §eVerlasse das Tutorial" );
        Language.DEUTSCH.add( "sw.command.tutorial.exists", "§cDas Tutorial exisitert bereits." );
        Language.DEUTSCH.add( "sw.command.tutorial.create", "§eDu haste das Tutorial erstellt." );
        Language.DEUTSCH.add( "sw.command.tutorial.create.addspawn", "§eVerwende /sw tutorial addSpawn§8| §e#1" );
        Language.DEUTSCH.add( "sw.command.tutorial.create.build", "§eVerwende /sw tutorial build §8| §e#2" );
        Language.DEUTSCH.add( "sw.command.tutorial.creating", "§cDu erstellst bereits das Tutorial, beende dies mit /sw tutorial build." );
        Language.DEUTSCH.add( "sw.command.tutorial.addspawn", "§eDu hast den Teleportationspunkt mit der ID §c%s §ehinzugefügt" );
        Language.DEUTSCH.add( "sw.command.tutorial.notcreating", "§cDu erstellst derzeit nicht das Tutorial." );
        Language.DEUTSCH.add( "sw.command.tutorial.build", "§eDu hast erfolgreich das §cTutorial §egespeichert." );

        Language.DEUTSCH.add( "tutorial.sailors.item_upgrade", "§6Hier können sich die Seeleute je nach Kills der Piraten Item-Upgrades abholen.\n" +
                "§6Sammelt ein Seemann das Upgrade in Form einer Brustplatte ein, so erhalten die weiteren Seemänner dieses auch." );
        Language.DEUTSCH.add( "tutorial.pirates.item_upgrade", "§6Je nach abgegebenem Gold können sich die Piraten hier Upgrades besorgen.\n" +
                "§6Nachdem genügend Gold abgegeben wurde, erhalten alle Piraten das Upgrade.\n" +
                "§6Zum Abgeben des Golds müssen die Piraten mit dem Gold auf §cMarc §6, welcher nur alle 90 Sekunden erscheint, rechtsklicken." );
        Language.DEUTSCH.add( "tutorial.gold.villager.kill", "§6Um das Gold einsammeln zu können, muss der Villager vor dem Tor getötet werden." );
        Language.DEUTSCH.add( "tutorial.gold.collect", "§6Nachdem das Tor offen ist, kann nun durch einen Rechtsklick das Gold aufgesammelt werden.\n" +
                "§6Durch das Einsammeln von Gold werden die Piraten je nach Menge langsamer.\n" +
                "§6Nachdem das gesamte Gold abgegeben worden ist, müssen nur noch die Seeleute getötet werden." );
        Language.DEUTSCH.add( "tutorial.hostage", "§6Zu Beginn erhält ein zufälliger Pirat einen Schlüssel, welchen die Seeleute durch Töten dieses Piraten erhalten. \n" +
                "§6Sobald sie den Schlüssel erhalten haben, können sie die Tür zur Geisel öffnen, woraufhin nur noch die Piraten ausgeschaltet werden müssen." );

        Language.DEUTSCH.add( "tutorial.start.running", "§cDu bist bereits im Tutorial." );
        Language.DEUTSCH.add( "tutorial.remove.user", "§cDu hast das Tutorial beendet. Möchtest du es erneut spielen, kannst du es in den Einstellungen wieder aktivieren." );
        Language.DEUTSCH.add( "tutorial.players.notready", "§cEs sind nicht alle Spieler mit dem Tutorial fertig, wartet einen Moment bis alle fertig sind..." );
        Language.DEUTSCH.add( "tutorial.title", "§cDas Tutorial" );
        Language.DEUTSCH.add( "tutorial.subtitle", "§4startet" );
        Language.DEUTSCH.add( "tutorial.running", "§cDa derzeit User im Tutorial sind, startet die Runde erst nachdem sie dies beendet haben, wartet kurz..." );

        Language.DEUTSCH.add( "stats.stat.points", "Punkte" );
        Language.DEUTSCH.add( "stats.stat.gamesplayed", "Gespielte Spiele" );
        Language.DEUTSCH.add( "stats.stat.wins", "Gewonnene Spiele" );
        Language.DEUTSCH.add( "stats.stat.kills", "Kills" );
        Language.DEUTSCH.add( "stats.stat.deaths", "Tode" );
        Language.DEUTSCH.add( "stats.stat.kd", "K/T" );
        Language.DEUTSCH.add( "stats.stat.deathmatches", "Endkämpfe" );
        Language.DEUTSCH.add( "stats.stat.rank", "Globaler Rang" );
        Language.DEUTSCH.add( "stats.stat.gold", "Gold" );
        Language.DEUTSCH.add( "stats.stat.pirates", "Piraten" );
        Language.DEUTSCH.add( "stats.stat.sailors", "Seeleute" );

        Language.DEUTSCH.add( "villager.attack", "§4§lEin Villager wird angegriffen!" );
        Language.DEUTSCH.add( "villager.death.title", "§cVillager gestorben!" );
        Language.DEUTSCH.add( "villager.death.subtitle", "§4Es verbleiben §e%s §4Villager." );
        Language.DEUTSCH.add( "villager.death.subtitle.0", "§4Alle Villager wurden getötet." );
        Language.DEUTSCH.add( "villager.pirates.trade.goldinhand", "§7Du musst einen Goldblock in der Hand haben." );
        Language.DEUTSCH.add( "villager.pirates.trade.spawn", "§6Der Händler ist erschienen, ihr könnt jetzt das Gold abgeben!" );
        Language.DEUTSCH.add( "villager.pirates.trade.despawn", "§4Der Händler ist gegangen. §6Er ist erst in einer Minute wieder da." );

        /** Items of pirates */
        Language.DEUTSCH.add( "items.pirates.hook", "§cHaken" );
        Language.DEUTSCH.add( "items.pirates.knife", "§7Dolch" );
        Language.DEUTSCH.add( "items.pirates.colt.little", "§ckleiner Revolver" );
        Language.DEUTSCH.add( "items.pirates.saber", "§7Säbel" );
        Language.DEUTSCH.add( "items.pirates.colt.big", "§cgroßer Revolver" );
        Language.DEUTSCH.add( "items.pirates.grenade", "§4Granate" );
        Language.DEUTSCH.add( "items.pirates.gold_sword", "§6Goldenes Schwert" );

        Language.DEUTSCH.add( "items.pirates.leather_helm.torn", "§cZerissene Lederkappe" );
        Language.DEUTSCH.add( "items.pirates.leather_chestplate.torn", "§cZerissenes Lederhemd" );
        Language.DEUTSCH.add( "items.pirates.leather_leggings.torn", "§cZerissene Lederhose" );
        Language.DEUTSCH.add( "items.pirates.leather_boots.torn", "§cZerissene Lederschuhe" );
        Language.DEUTSCH.add( "items.pirates.leather_helm", "§cLederkappe" );
        Language.DEUTSCH.add( "items.pirates.leather_chestplate", "§cLederhemd" );
        Language.DEUTSCH.add( "items.pirates.leather_leggings", "§cLederhose" );
        Language.DEUTSCH.add( "items.pirates.leather_boots", "§cLederschuhe" );
        Language.DEUTSCH.add( "items.pirates.iron_helm", "§7Eisenhelm" );
        Language.DEUTSCH.add( "items.pirates.iron_chestplate", "§7Eisenbrust" );
        Language.DEUTSCH.add( "items.pirates.iron_leggings", "§7Eisenhose" );
        Language.DEUTSCH.add( "items.pirates.iron_boots", "§7Eisenstiefel" );
        Language.DEUTSCH.add( "items.pirates.gold_helm", "§6Goldhelm" );
        Language.DEUTSCH.add( "items.pirates.gold_chestplate", "§6Goldbrust" );
        Language.DEUTSCH.add( "items.pirates.gold_leggings", "§6Goldhose" );
        Language.DEUTSCH.add( "items.pirates.gold_boots", "§6Goldstiefel" );

        Language.DEUTSCH.add( "items.pirates.key", "§cSchlüssel" );
        Language.DEUTSCH.add( "items.pirates.key.received", "§cDu hast den Schlüssel erhalten!" );
        Language.DEUTSCH.add( "items.pirates.key.player.received", "§c%s §7hat den Schlüssel erhalten!" );
        Language.DEUTSCH.add( "items.pirates.upgrade", "§6Du hast ein Item-Upgrade erhalten!" );

        Language.DEUTSCH.add( "items.settings.name", "§cEinstellungen" );
        Language.DEUTSCH.add( "inventory.settings.title", "Einstellungen" );
        Language.DEUTSCH.add( "items.settings.tutorial.on", "§eTutorial machen: §6Ja" );
        Language.DEUTSCH.add( "items.settings.tutorial.off", "§eTutorial machen: §cNein" );

        /** Items of sailors */
        Language.DEUTSCH.add( "items.sailors.compass", "§cKompass" );
        Language.DEUTSCH.add( "items.sailors.pitchfork", "§cMistgabel" );
        Language.DEUTSCH.add( "items.sailors.colt", "§cAlter Revolver" );
        Language.DEUTSCH.add( "items.sailors.knife", "§8Messer" );
        Language.DEUTSCH.add( "items.sailors.musket", "§7Muskete" );
        Language.DEUTSCH.add( "items.sailors.dagger", "§cDolch" );
        Language.DEUTSCH.add( "items.sailors.shotgun", "§4Donnerbüchse" );
        Language.DEUTSCH.add( "items.sailors.axe.throwing", "§cWurfaxt" );

        Language.DEUTSCH.add( "items.sailors.leather_helm", "§cLederkappe" );
        Language.DEUTSCH.add( "items.sailors.leather_chestplate", "§cLederhemd" );
        Language.DEUTSCH.add( "items.sailors.leather_leggings", "§cLederhose" );
        Language.DEUTSCH.add( "items.sailors.leather_boots", "§cLederschuhe" );
        Language.DEUTSCH.add( "items.sailors.gold_helm", "§6Goldhelm" );
        Language.DEUTSCH.add( "items.sailors.gold_chestplate", "§6Goldbrust" );
        Language.DEUTSCH.add( "items.sailors.gold_leggings", "§6Goldhose" );
        Language.DEUTSCH.add( "items.sailors.gold_boots", "§6Goldstiefel" );
        Language.DEUTSCH.add( "items.sailors.iron_helm", "§7Eisenhelm" );
        Language.DEUTSCH.add( "items.sailors.iron_chestplate", "§7Eisenbrust" );
        Language.DEUTSCH.add( "items.sailors.iron_leggings", "§7Eisenhose" );
        Language.DEUTSCH.add( "items.sailors.iron_boots", "§7Eisenstiefel" );
        Language.DEUTSCH.add( "items.sailors.diamond_helm", "§bDiamantenhelm" );
        Language.DEUTSCH.add( "items.sailors.diamond_chestplate", "§bDiamantenbrust" );
        Language.DEUTSCH.add( "items.sailors.diamond_leggings", "§bDiamantenhose" );
        Language.DEUTSCH.add( "items.sailors.diamond_boots", "§bDiamantenstiefel" );

        Language.DEUTSCH.add( "sailors.upgrade.available", "§6Ihr könnt euch ein Upgrade holen!" );
        Language.DEUTSCH.add( "sailors.upgraded", "§6Ihr habt ein Upgrade erhalten!" );

        Language.DEUTSCH.add( "match.minutesinfo", "§7Das Spiel endet in §c%s §7Minuten." );
        Language.DEUTSCH.add( "match.secondsinfo", "§7Das Spiel endet in §c%s §7Sekunden." );

        Language.DEUTSCH.add( "scoreboard.teamsforbidden", "§cVerboten" );
        Language.DEUTSCH.add( "scoreboard.line.1", "§7Eingesammeltes Gold§8: §e%s" );
        Language.DEUTSCH.add( "scoreboard.line.2", "§7Lebende Villager§8: §e%s§8/§e3" );
        Language.DEUTSCH.add( "scoreboard.line.3", "§7Geisel befreit§8: §e%s" );
        Language.DEUTSCH.add( "scoreboard.line.4", "§7Items abholbar in§8: §e%s Minuten" );
        Language.DEUTSCH.add( "scoreboard.line.5", "§7Piraten§8: §e%s" );
        Language.DEUTSCH.add( "scoreboard.line.6", "§7Seeleute§8: §e%s" );

        Language.DEUTSCH.add( "map.nogold", "§4Es ist kein Gold auf der Map, melde den Fehler bitte an das Team." );
        Language.DEUTSCH.add( "map.prepare", "§4Die Map wird vorbereitet..." );

        Language.DEUTSCH.add( "gold.collected", "§c%s §7hat Gold gesammelt" );
        Language.DEUTSCH.add( "gold.collected.all", "§cDas gesamte Gold wurde gesammelt! Die Seeleute können nun endgültig getötet werden!" );
        Language.DEUTSCH.add( "gold.collecting", "§cDu packst derzeit bereits Gold ein." );
        Language.DEUTSCH.add( "gold.collect", "§eDu kannst wieder Gold sammeln." );
        Language.DEUTSCH.add( "gold.saved", "§c%s §7hat Gold gesichert." );

        Language.DEUTSCH.add( "hostage.delivered.title", "§eGeisel befreit" );
        Language.DEUTSCH.add( "hostage.delivered.subtitle", "§cvon §e%s" );
        Language.DEUTSCH.add( "hostage.delivered.pirates.kill", "§4Die Piraten können jetzt endgültig sterben." );

        Language.DEUTSCH.add( "weapon.loading", "§cDeine Waffe läd nach..." );

        Language.DEUTSCH.add( "compass.settings.title", "§e§lFixierung des Kompass!" );
        Language.DEUTSCH.add( "compass.key", "§eKompass auf Schlüssel fixieren" );
        Language.DEUTSCH.add( "compass.key.message", "§eDu hast den Kompass auf den Schlüssel fixiert." );
        Language.DEUTSCH.add( "compass.hostage", "§eKompass auf Geisel fixieren" );
        Language.DEUTSCH.add( "compass.hostage.message", "§eDu hast den Kompass auf die Geisel fixiert." );

        /** English-messages */
        Language.ENGLISH.add( "nopermissions", "§cYou do not have permissions to perform this command." );
        Language.ENGLISH.add( "sw.command.arena.creating", "§cYou're already creating a arena, cancel this with /sw create build." );
        Language.ENGLISH.add( "sw.command.arena.help.1", "§e/sw arena create <Name> §8| §eCreate a new arena" );
        Language.ENGLISH.add( "sw.command.arena.help.2", "§e/sw build §8| §eGet the permission to rebuild the arena" );
        Language.ENGLISH.add( "sw.command.arena.create", "§eYou've created a new arena: %s" );
        Language.ENGLISH.add( "sw.command.arena.addspawn", "§eUse /sw arena addSpawn <Type> <ID> §8| §e#1" );
        Language.ENGLISH.add( "sw.command.arena.setspecspawn", "§eUse /sw arena setspecspawn §8| §e#2" );
        Language.ENGLISH.add( "sw.command.arena.build", "§eUse /sw arena build §8| §e#3" );
        Language.ENGLISH.add( "sw.command.arena.build.success", "§eYou've now permissions to build." );
        Language.ENGLISH.add( "sw.command.arena.build.remove", "§4You've no more permissions to build." );
        Language.ENGLISH.add( "sw.command.arena.addspawn", "§eYou've created a spawn of the type §c%s §ewith the ID §c%s" );
        Language.ENGLISH.add( "sw.command.arena.notcreating", "§cCurrently you're not creating a arena." );
        Language.ENGLISH.add( "sw.command.arena.setspectatorspawn", "§eYou've created the spectator spawn for §c%s §e." );
        Language.ENGLISH.add( "sw.command.arena.build", "§eYou've successfully saved the arena §c%s." );
        Language.ENGLISH.add( "sw.command.arena.exists", "§cThe arena already exists." );

        Language.ENGLISH.add( "scoreboard.teamsforbidden", "§cNo, don't do this! :(" ); //easteregg? :D

        Language.ENGLISH.add( "stats.stat.points", "Points" );
        Language.ENGLISH.add( "stats.stat.gamesplayed", "Played games" );
        Language.ENGLISH.add( "stats.stat.wins", "Games won" );
        Language.ENGLISH.add( "stats.stat.kills", "Kills" );
        Language.ENGLISH.add( "stats.stat.deaths", "Deaths" );
        Language.ENGLISH.add( "stats.stat.kd", "K/D" );
        Language.ENGLISH.add( "stats.stat.deathmatches", "Deathmatches" );
        Language.ENGLISH.add( "stats.stat.rank", "Global Rank" );

    }
}
