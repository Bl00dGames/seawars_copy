package com.playminity.seawars.gamephase;

import com.playminity.minigameframework.handler.GameHandler;
import com.playminity.minigameframework.handler.phase.DefaultBeginPhase;
import com.playminity.seawars.Data;
import com.playminity.seawars.SWGameHandler;
import com.playminity.seawars.SeaWars;
import com.playminity.seawars.utils.SWAchievements;
import com.playminity.seawars.utils.SWStats;
import com.playminity.seawars.utils.WorldUtil;
import com.playminity.seawars.utils.scoreboard.GameScoreboard;
import com.playminity.seawars.utils.teams.TeamItems;
import com.playminity.seawars.utils.teams.TeamManager;
import me.kombustorlp.gameapi.GameAPI;
import me.kombustorlp.gameapi.languagesystem.Language;
import me.kombustorlp.gameapi.misc.User;
import org.bukkit.GameMode;

/**
 * Created by Andr� on 03.10.2016.
 */
public class BeginPhase extends DefaultBeginPhase {

    public BeginPhase ( ) {

        super( Data.getConfig( ).getBeginTime( ), true, GameMode.ADVENTURE );

    }

    @Override
    public void onBegin ( ) {

        GameScoreboard gameScoreboard = new GameScoreboard( );
        gameScoreboard.startUpdate( );

        SeaWars.getInstance( ).getFightPhase( ).setScoreboard( gameScoreboard );

        GameHandler.getBaseInstance( ).getIngamePlayers( ).forEach( player -> {

            User user = GameAPI.getUser( player );
            Language language = user.getLanguage( );

            player.setScoreboard( gameScoreboard.getScoreboard( language ) );
            player.getInventory( ).clear( );

            SWGameHandler.getInstance( ).setMoveAllowed( player, true );
            SWGameHandler.getBaseInstance( ).setMoveAllowed( player, true );

            SeaWars.getStatsModule( ).add( user, SWStats.GAMESPLAYED, 1 );
            user.callAchievement( SWAchievements.FIRSTGAME );

            if ( SWGameHandler.getInstance( ).getTeamModule( ).getTeam( user.getPlayer( ) ).getIndex( ) == 1 ) {

                SeaWars.getStatsModule( ).add( user, SWStats.SAILORS, 1 );
            } else {

                SeaWars.getStatsModule( ).add( user, SWStats.PIRATES, 1 );
            }

            TeamItems.giveTeamItems( player );
        } );

        TeamItems.giveRandomKey( );

        WorldUtil.setupMap( );

        TeamManager.startCommpassSchedule( );
    }
}
