package com.playminity.seawars.gamephase;

import com.playminity.minigameframework.MGFramework;
import com.playminity.minigameframework.countdown.Countdown;
import com.playminity.minigameframework.handler.GameHandler;
import com.playminity.minigameframework.handler.phase.DefaultLobbyPhase;
import com.playminity.seawars.Data;
import com.playminity.seawars.SWGameHandler;
import com.playminity.seawars.SeaWars;
import com.playminity.seawars.arenasystem.SWArenaHandler;
import com.playminity.seawars.utils.WorldUtil;
import com.playminity.seawars.utils.teams.TeamManager;
import me.kombustorlp.gameapi.GameAPI;
import me.kombustorlp.gameapi.languagesystem.Language;
import me.kombustorlp.gameapi.libraries.ItemUtils;
import me.kombustorlp.gameapi.misc.User;
import me.kombustorlp.gameapi.utils.WorldUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Andr� on 03.10.2016.
 */
public class LobbyPhase extends DefaultLobbyPhase {


    private int pirates = 0;
    private int sailors = 0;

    public LobbyPhase ( ) {

        super( Data.getConfig( ).getTeleportTime( ), Data.getConfig( ).getTeleportSeconds( ), true, false, true );
        getCountdown( ).addEvent( new Countdown.CountdownEvent( ) {
            @Override
            public void run ( ) {

                if ( Data.getVotedArena( ) == null ) {
                    Data.setVotedArena( SeaWars.getInstance( ).getVoteModule( ).drawVotedMap( ) );
                    MGFramework.debug( "Drawing voted map: " + Data.getVotedArena( ).getName( ) );


                }
            }
        }, 10 );

        getCountdown( ).addEvent( ( ) -> {

            MGFramework.debug( "SW Map: " + Data.getVotedArena( ).getData( ).getWorld( ) );
            WorldUtils.setDisablePendingBlocksTick( Data.getVotedArena( ).getData( ).getWorld( ), false );
            WorldUtils.setDisableBlockTicks( Data.getVotedArena( ).getData( ).getWorld( ), false );

            Language.broadcast( SWGameHandler.getInstance( ).getChatPrefix( ), "map.prepare" );

            HashMap< Location, Block > blockMap = new HashMap< Location, Block >( );

            for ( int i = 1; i <= 3; i++ ) {

                WorldUtil.getBlocksOfMaterial( Data.getVotedArena( ).getData( ).getSpawn( "villager", i ).getLocation( ), 25, Material.GOLD_BLOCK, blockMap );
            }

            SWArenaHandler.getInstance( ).setBlockList( blockMap );

            SWArenaHandler.getInstance( ).setGold( SWArenaHandler.getInstance( ).getBlockList( ).size( ) );

            if ( SWArenaHandler.getInstance( ).getGold( ) == 0 ) {
                Language.broadcast( SWGameHandler.getInstance( ).getChatPrefix( ), "map.nogold" );
                setNextPhase( SeaWars.getInstance( ).getEndPhase( ) );
                stop( true );
            }

            TeamManager.onTeamBlancing( );
            List< Player > playerList = new ArrayList<>( );
            Bukkit.getOnlinePlayers( ).forEach( player -> {

                if ( SeaWars.getInstance( ).getTutorialManager( ).playTutorial( GameAPI.getUser( player ) ) ) {

                    playerList.add( player );
                }
            } );

            if ( playerList.size( ) == 0 ) {

                playerList.clear( );

                setNextPhase( SeaWars.getInstance( ).getBeginPhase( ) );
                return;
            }

            setNextPhase( SeaWars.getInstance( ).getTutorialPhase( ) );
            playerList.clear( );
        }, 0 );
    }

    public Location getSpawn ( String index, int i ) {

        return Data.getVotedArena( ).getData( ).getSpawn( index, i ).getLocation( );
    }

    @Override
    public Location getNextSpawn ( int i, Player player ) {

        User user = GameAPI.getUser( player );

        if ( SWGameHandler.getInstance( ).getTeamModule( ).getTeam( user.getPlayer( ) ).getIndex( ) == 1 ) {

            sailors++;
            return Data.getVotedArena( ).getData( ).getSpawn( "sailors", sailors ).getLocation( );
        }

        if ( SWGameHandler.getInstance( ).getTeamModule( ).getTeam( user.getPlayer( ) ).getIndex( ) == 2 ) {

            pirates++;
            return Data.getVotedArena( ).getData( ).getSpawn( "pirates", pirates ).getLocation( );
        }

        return null;
    }

    @Override
    public void onTeleport ( Player player ) {

        GameHandler.getBaseInstance( ).setMoveAllowed( player, false );
    }

    @Override
    public void onPlayerJoin ( Player p, PlayerJoinEvent e ) {

        super.onPlayerJoin( p, e );

        p.getInventory( ).setItem( 7, ItemUtils.createItemStack( Material.REDSTONE_COMPARATOR, GameAPI.getUser( p ).getLanguage( ).get( "items.settings.name" ) ) );

        if ( ! SeaWars.getInstance( ).getTutorialManager( ).hasSetting( GameAPI.getUser( p ) ) ) {

            SeaWars.getInstance( ).getTutorialManager( ).setPlayTutorial( GameAPI.getUser( p ), true );
        }

        p.updateInventory( );
    }
}
