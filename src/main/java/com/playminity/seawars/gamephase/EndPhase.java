package com.playminity.seawars.gamephase;

import com.playminity.minigameframework.handler.phase.DefaultEndPhase;

/**
 * Created by Andr� on 03.10.2016.
 */
public class EndPhase extends DefaultEndPhase {

    public EndPhase ( ) {

        super( true );
    }
}
