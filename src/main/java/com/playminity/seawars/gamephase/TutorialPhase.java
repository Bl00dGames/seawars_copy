package com.playminity.seawars.gamephase;

import com.playminity.minigameframework.countdown.Countdown;
import com.playminity.minigameframework.countdown.CountdownOption;
import com.playminity.minigameframework.handler.GameHandler;
import com.playminity.minigameframework.handler.phase.GamePhase;
import com.playminity.minigameframework.utils.GameState;
import com.playminity.seawars.SWGameHandler;
import com.playminity.seawars.SeaWars;
import me.kombustorlp.gameapi.GameAPI;
import me.kombustorlp.gameapi.languagesystem.Language;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by André on 13.11.2016.
 */
public class TutorialPhase extends GamePhase {


    public TutorialPhase ( ) {

        super( "tutorial", GameState.PREGAME );
        setCountdown( new Countdown( 71, 1, 0, CountdownOption.XP_BAR_INFORMATION ) {

            @Override
            public void onDecrease ( int i ) {

            }

            @Override
            public void onFinish ( ) {

            }

            @Override
            public void start ( ) {

                super.start( );
                Language.broadcast( SWGameHandler.getInstance( ).getChatPrefix( ), "tutorial.running" );
            }
        } );

        getCountdown( ).addEvent( new Countdown.CountdownEvent( ) {
            @Override
            public void run ( ) {

                Bukkit.getOnlinePlayers( ).forEach( player -> {

                    if ( SeaWars.getInstance( ).getTutorialManager( ).playTutorial( GameAPI.getUser( player ) ) ) {

                        GameHandler.getBaseInstance( ).setMoveAllowed( player, true );
                        SeaWars.getInstance( ).getTutorialManager( ).addUserToQueue( GameAPI.getUser( player ) );
                    } else {

                        GameHandler.getBaseInstance( ).setMoveAllowed( player, false );
                    }
                } );
            }
        }, 60 );

        getCountdown( ).addEvent( new Countdown.CountdownEvent( ) {
            @Override
            public void run ( ) {

                setNextPhase( SeaWars.getInstance( ).getBeginPhase( ) );
                stop( true );
            }
        }, 0 );
    }

    @Override
    public void onPlayerJoin ( Player player, PlayerJoinEvent playerJoinEvent ) {

    }

    @Override
    public void onPlayerLeave ( Player player, PlayerQuitEvent playerQuitEvent ) {

    }
}
