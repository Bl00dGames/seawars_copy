package com.playminity.seawars.gamephase;

import com.playminity.minigameframework.handler.phase.DefaultFightPhase;
import com.playminity.seawars.Data;
import com.playminity.seawars.SWGameHandler;
import com.playminity.seawars.utils.WorldUtil;
import me.kombustorlp.gameapi.languagesystem.Language;
import org.bukkit.Bukkit;
import org.bukkit.Instrument;
import org.bukkit.Note;
import org.bukkit.Sound;

/**
 * Created by Andr� on 03.10.2016.
 */
public class FightPhase extends DefaultFightPhase {

    public FightPhase ( ) {

        super( "fight", Data.getConfig( ).getIngameTime( ), true, true );

        getCountdown( ).addEvent( ( ) -> {
            Language.broadcast( SWGameHandler.getInstance( ).getChatPrefix( ), "match.minutesinfo", getCountdown( ).getCurrentValue( ) / 60 );
        }, 5 * 60, 3 * 60, 2 * 60, 60 );

        getCountdown( ).addEvent( ( ) -> {
            Language.broadcast( SWGameHandler.getInstance( ).getChatPrefix( ), "match.secondsinfo", getCountdown( ).getCurrentValue( ) );
        }, 30 );

        getCountdown( ).addEventRange( 10, 1, ( ) -> {
            Language.broadcast( SWGameHandler.getInstance( ).getChatPrefix( ), "match.secondsinfo", getCountdown( ).getCurrentValue( ) );
            Bukkit.getOnlinePlayers( ).forEach( player -> player.playNote( player.getLocation( ), Instrument.PIANO, new Note( 0 ) ) );
        } );

        getCountdown( ).addEvent( ( ) -> {
            WorldUtil.spawnPirateVillager( );
        }, 540, 450, 360, 270, 180, 90 );

        getCountdown( ).addEvent( ( ) -> {
            WorldUtil.despawnPirateVillager( );
        }, 510, 420, 330, 240, 150 );
    }

    @Override
    public void start ( ) {

        super.start( );
    }

    @Override
    public void onEnd ( ) {

        SWGameHandler.getInstance( ).setPVP( false );
        Bukkit.getOnlinePlayers( ).forEach( player -> player.playSound( player.getLocation( ), Sound.LEVEL_UP, 5L, 5L ) );
    }

    @Override
    public void onDecrease ( int i ) {

    }
}
