package com.playminity.seawars.commands;

import com.playminity.seawars.Data;
import com.playminity.seawars.SWGameHandler;
import com.playminity.seawars.arenasystem.SWArenaData;
import com.playminity.seawars.arenasystem.SWArenaHandler;
import com.playminity.seawars.arenasystem.SWCreateArena;
import com.playminity.seawars.arenasystem.SWCreateTArena;
import me.kombustorlp.gameapi.GameAPI;
import me.kombustorlp.gameapi.arenamanager.Arena;
import me.kombustorlp.gameapi.arenamanager.LocationWrapper;
import me.kombustorlp.gameapi.misc.Rang;
import me.kombustorlp.gameapi.misc.User;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by André on 03.10.2016.
 */
public class SWCommand implements CommandExecutor {

    private SWCreateArena swCreateArena;
    private SWCreateTArena swCreateTArena;

    @Override
    public boolean onCommand ( CommandSender commandSender, Command command, String label, String[] arguments ) {

        if ( ! ( commandSender instanceof Player ) ) {

            commandSender.sendMessage( "You have to be a player!" );
            return true;
        }

        User user = GameAPI.getUser( ( ( Player ) commandSender ) );

        if ( user.getRang( ).isLowerAs( Rang.DEVELOPER ) ) {
            user.sendMessage( SWGameHandler.getBaseInstance( ).getChatPrefix( ), "nopermissions" );

            return true;
        }

        if ( arguments.length == 0 ) {

            user.sendMessage( SWGameHandler.getBaseInstance( ).getChatPrefix( ), "sw.command.arena.help.1" );
            user.sendMessage( SWGameHandler.getBaseInstance( ).getChatPrefix( ), "sw.command.arena.help.2" );

            user.sendMessage( SWGameHandler.getBaseInstance( ).getChatPrefix( ), "sw.command.tutorial.help.1" );
            return true;
        }

        if ( arguments.length == 1 ) {

            if ( arguments[ 0 ].equalsIgnoreCase( "build" ) ) {

                if ( user.getPlayer( ).getGameMode( ) == GameMode.CREATIVE ) {

                    user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "sw.command.arena.build.remove" );

                    user.getPlayer( ).setGameMode( GameMode.ADVENTURE );

                    user.getPlayer( ).getWorld( ).save( );

                    return true;
                }

                user.getPlayer( ).setGameMode( GameMode.CREATIVE );

                user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "sw.command.arena.build.success" );
            }
        }

        if ( arguments.length == 2 ) {

            if ( arguments[ 0 ].equalsIgnoreCase( "tutorial" ) ) {

                if ( arguments[ 1 ].equalsIgnoreCase( "create" ) ) {

                    if ( swCreateTArena != null ) {

                        user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "sw.command.tutorial.creating" );
                        return true;
                    }

                    if ( Data.getTutorialArena( ) != null ) {

                        user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "sw.command.tutorial.exists" );
                        return true;
                    }

                    swCreateTArena = new SWCreateTArena( );

                    user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "sw.command.tutorial.create" );
                    user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "sw.command.tutorial.create.addspawn" );
                    user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "sw.command.tutorial.create.build" );
                    return true;
                }

                if ( arguments[ 1 ].equalsIgnoreCase( "build" ) ) {

                    if ( swCreateTArena == null ) {

                        user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "sw.command.tutorial.notcreating" );
                        return true;
                    }

                    swCreateTArena.store( );

                    user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "sw.command.tutorial.build" );

                    swCreateTArena = null;
                    return true;
                }
            }

            if ( arguments[ 0 ].equalsIgnoreCase( "arena" ) ) {

                if ( arguments[ 1 ].equalsIgnoreCase( "build" ) ) {

                    if ( swCreateArena == null ) {

                        user.sendMessage( SWGameHandler.getBaseInstance( ).getChatPrefix( ), "sw.command.arena.notcreating" );
                        return true;
                    }

                    swCreateArena.store( );

                    user.sendMessage( SWGameHandler.getBaseInstance( ).getChatPrefix( ), "sw.command.arena.build", swCreateArena.getName( ) );

                    swCreateArena = null;
                    return true;
                }

                if ( arguments[ 1 ].equalsIgnoreCase( "setspecspawn" ) ) {

                    if ( swCreateArena == null ) {

                        user.sendMessage( SWGameHandler.getBaseInstance( ).getChatPrefix( ), "sw.command.arena.notcreating" );
                        return true;
                    }

                    swCreateArena.getData( ).setSpectatorSpawn( new LocationWrapper( user.getPlayer( ).getLocation( ) ) );
                    user.sendMessage( SWGameHandler.getBaseInstance( ).getChatPrefix( ), "sw.command.arena.setspectatorspawn", swCreateArena.getName( ) );
                    return true;
                }
            }
        }

        if ( arguments.length == 3 ) {

            if ( arguments[ 0 ].equalsIgnoreCase( "arena" ) ) {

                if ( arguments[ 1 ].equalsIgnoreCase( "create" ) ) {

                    if ( swCreateArena != null ) {

                        user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "sw.command.arena.creating" );
                        return true;
                    }

                    for ( Arena< SWArenaData > swArenaDataArena : SWArenaHandler.getInstance( ).getArenaHandler( ).getArenas( ) ) {

                        if ( swArenaDataArena.getName( ).equalsIgnoreCase( arguments[ 2 ] ) ) {

                            user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "sw.command.arena.exists" );
                            return true;
                        }
                    }

                    swCreateArena = new SWCreateArena( arguments[ 2 ] );

                    user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "sw.command.arena.create", swCreateArena.getName( ) );
                    user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "sw.command.arena.addspawn" );
                    user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "sw.command.arena.setspecspawn" );
                    user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "sw.command.arena.build" );
                    return true;
                }
            }

            if ( arguments[ 0 ].equalsIgnoreCase( "tutorial" ) ) {

                if ( arguments[ 1 ].equalsIgnoreCase( "addSpawn" ) ) {

                    if ( swCreateTArena == null ) {

                        user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "sw.command.tutorial.notcreating" );
                        return true;
                    }

                    swCreateTArena.getData( ).addSpawn( swCreateTArena.getData( ).getSpawns( ).size( ) + 1, user.getPlayer( ).getLocation( ), arguments[ 2 ] );

                    user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "sw.command.tutorial.addspawn", swCreateTArena.getData( ).getSpawns( ).size( ) );

                    return true;
                }
            }
        }

        if ( arguments.length == 4 ) {

            if ( ! arguments[ 0 ].equalsIgnoreCase( "arena" ) ) return true;
            if ( ! arguments[ 1 ].equalsIgnoreCase( "addSpawn" ) ) return true;

            swCreateArena.getData( ).addSpawn( arguments[ 2 ] + "." + arguments[ 3 ], user.getPlayer( ).getLocation( ) );

            user.sendMessage( SWGameHandler.getBaseInstance( ).getChatPrefix( ), "sw.command.arena.addspawn", arguments[ 2 ], arguments[ 3 ] );

            return true;
        }

        return true;
    }
}
