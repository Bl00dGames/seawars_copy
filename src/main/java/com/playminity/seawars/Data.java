package com.playminity.seawars;

import com.playminity.seawars.arenasystem.SWArenaData;
import com.playminity.seawars.arenasystem.SWTArenaData;
import com.playminity.seawars.utils.SWConfig;
import lombok.Getter;
import lombok.Setter;
import me.kombustorlp.gameapi.arenamanager.Arena;

/**
 * Created by Andr� on 28.09.2016.
 */
public class Data {

    @Getter
    @Setter
    private static SWConfig config;

    @Getter
    @Setter
    private static Arena< SWArenaData > votedArena;

    @Getter
    @Setter
    private static Arena< SWTArenaData > tutorialArena;
}
