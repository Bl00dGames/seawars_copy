package com.playminity.seawars.listener;

import com.playminity.minigameframework.modules.TeamModule;
import com.playminity.seawars.SWGameHandler;
import com.playminity.seawars.SeaWars;
import com.playminity.seawars.utils.teams.TeamItems;
import com.playminity.seawars.utils.teams.TeamManager;
import me.kombustorlp.gameapi.GameAPI;
import me.kombustorlp.gameapi.libraries.ItemUtils;
import me.kombustorlp.gameapi.libraries.inventories.ClickEvent;
import me.kombustorlp.gameapi.libraries.inventories.Inventories;
import me.kombustorlp.gameapi.libraries.inventories.InventoryListener;
import me.kombustorlp.gameapi.misc.User;
import org.bukkit.Material;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerEggThrowEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by André on 04.11.2016.
 */
public class ItemListener implements Listener {

    private List< Player > playerList = new ArrayList<>( );

    @EventHandler
    public void onInteract ( PlayerInteractEvent event ) {


        if ( event.getAction( ) != Action.RIGHT_CLICK_AIR && event.getAction( ) != Action.RIGHT_CLICK_BLOCK ) {

            return;
        }

        if ( event.getItem( ) == null || event.getItem( ).getType( ) == null || event.getItem( ).getType( ) == Material.AIR ) {
            return;
        }


        event.getItem( ).setDurability( new ItemStack( event.getItem( ).getType( ) ).getDurability( ) );

        Player player = event.getPlayer( );

        if ( event.getItem( ).getType( ) == Material.REDSTONE_COMPARATOR ) {

            SeaWars.getInstance( ).getTutorialManager( ).openSettings( GameAPI.getUser( player ) );
        }

        if ( event.getItem( ).getType( ) == Material.COMPASS ) {

            Inventory inventory = Inventories.newListenableInventory( "§e" + GameAPI.getUser( player ).getLanguage( ).get( "compass.settings.title" ), 9, new InventoryListener( ) {

                @Override
                public void click ( ClickEvent clickEvent, User user ) {

                    clickEvent.setCanceled( true );
                    user.getPlayer( ).closeInventory( );

                    if ( clickEvent.getCurrentItem( ).getType( ) == Material.TRIPWIRE_HOOK ) {

                        TeamManager.getUserCompassTypeMap( ).put( user, TeamManager.CompassType.KEY );
                        user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "compass.key.message" );
                        return;
                    }

                    if ( clickEvent.getCurrentItem( ).getType( ) == Material.EMERALD ) {

                        TeamManager.getUserCompassTypeMap( ).put( user, TeamManager.CompassType.HOSTAGE );
                        user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "compass.hostage.message" );
                        return;
                    }
                }
            } );

            for ( int i = 0; i < 9; i++ ) {
                inventory.setItem( i, ItemUtils.createItemStack( Material.STAINED_GLASS_PANE, " " ) );
            }

            inventory.setItem( 3, ItemUtils.createItemStack( Material.EMERALD, GameAPI.getUser( player ).getLanguage( ).get( "compass.hostage" ) ) );
            inventory.setItem( 5, ItemUtils.createItemStack( Material.TRIPWIRE_HOOK, GameAPI.getUser( player ).getLanguage( ).get( "compass.key" ) ) );

            player.openInventory( inventory );
        }

        if ( playerList.contains( player ) ) {

            GameAPI.getUser( player ).sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "weapon.loading" );
            event.setCancelled( true );
            return;
        }

        /** Sailors */
            /* Colt */
        if ( event.getItem( ).getType( ) == Material.WOOD_SPADE ) {

            player.throwSnowball( );
            addCooldown( player );
        }

            /* Musket */
        if ( event.getItem( ).getType( ) == Material.WOOD_AXE ) {

            player.throwSnowball( );
            addCooldown( player );
        }

            /* Shotgun */
        if ( event.getItem( ).getType( ) == Material.BLAZE_ROD ) {

            player.throwSnowball( );
            player.throwSnowball( );
            player.throwSnowball( );
            player.throwSnowball( );
            addCooldown( player );
        }

            /* Throwing axe */
        if ( event.getItem( ).getType( ) == Material.IRON_AXE ) {
            final Item grenade = player.getWorld( ).dropItem( player.getEyeLocation( ), event.getItem( ) );
            player.getInventory( ).remove( event.getItem( ) );

            Vector currentVelocity = player.getLocation( ).getDirection( );

            grenade.setVelocity( currentVelocity.multiply( 1.5D ) );

            BukkitTask bukkitTask = new BukkitRunnable( ) {

                @Override
                public void run ( ) {

                    for ( Entity entity : grenade.getNearbyEntities( 2, 2, 2 ) ) {
                        if ( entity.getType( ) == EntityType.PLAYER ) {
                            Player player1 = ( ( Player ) entity );
                            player1.damage( 6.0 );
                            grenade.remove( );
                            cancel( );
                        }
                    }
                }
            }.runTaskTimerAsynchronously( SeaWars.getInstance( ), 20, 20 );

            new BukkitRunnable( ) {

                @Override
                public void run ( ) {

                    if ( grenade != null ) {

                        grenade.remove( );
                        bukkitTask.cancel( );
                    }
                }
            }.runTaskLaterAsynchronously( SeaWars.getInstance( ), 100L );
        }

        /** Pirates */
            /* Small Colt */
        if ( event.getItem( ).getType( ) == Material.STONE_SPADE ) {

            player.throwSnowball( );
            addCooldown( player );
        }

            /* Big Colt */
        if ( event.getItem( ).getType( ) == Material.IRON_SPADE ) {

            player.throwSnowball( );
            addCooldown( player );
        }
    }

    @EventHandler
    public void onProjectileHit ( EntityDamageByEntityEvent event ) {

        if ( ! ( event.getDamager( ) instanceof Projectile && event.getEntity( ) instanceof Player ) ) {
            return;
        }

        Projectile projectile = ( ( Projectile ) event.getDamager( ) );

        if ( ! ( projectile.getShooter( ) instanceof Player ) ) {

            return;
        }

        Player shooter = ( ( Player ) projectile.getShooter( ) );
        User shooterUser = GameAPI.getUser( shooter );

        Player player = ( ( Player ) event.getEntity( ) );
        User palyerUser = GameAPI.getUser( player );

        TeamModule teamModule = SWGameHandler.getInstance( ).getTeamModule( );

        if ( teamModule.getTeam( shooter ) == teamModule.getTeam( player ) ) {

            return;
        }

        event.setCancelled( true );

        /** Sailors */
        if ( teamModule.getTeam( shooter ).getIndex( ) == 1 ) {

                /* Colt */
            if ( TeamItems.containsItem( shooter, Material.WOOD_SPADE ) ) {

                player.damage( 4.0, shooter );
            }

                /* Musket */
            if ( TeamItems.containsItem( shooter, Material.WOOD_AXE ) ) {

                player.damage( 6.0, shooter );
            }

                /* Shotgun */
            if ( TeamItems.containsItem( shooter, Material.BLAZE_ROD ) ) {

                player.damage( 8.0, shooter );
            }
        }

        /** Pirates */
        if ( teamModule.getTeam( shooter ).getIndex( ) == 2 ) {

                /* Small Colt */
            if ( TeamItems.containsItem( shooter, Material.STONE_SPADE ) ) {

                player.damage( 5.0, shooter );
            }

                /* Big Colt */
            if ( TeamItems.containsItem( shooter, Material.IRON_SPADE ) ) {

                player.damage( 8.0, shooter );
            }
        }
    }

    @EventHandler
    public void onEggThrow ( PlayerEggThrowEvent event ) {

        event.getEgg( ).getWorld( ).createExplosion( event.getEgg( ).getLocation( ), 5 );

    }

    @EventHandler
    public void onBlockDamage ( BlockExplodeEvent event ) {

        event.blockList( ).clear( );
    }

    private void addCooldown ( Player player ) {

        playerList.add( player );

        new BukkitRunnable( ) {
            @Override
            public void run ( ) {

                playerList.remove( player );
            }
        }.runTaskLater( SeaWars.getInstance( ), 2 * 20 );
    }
}
