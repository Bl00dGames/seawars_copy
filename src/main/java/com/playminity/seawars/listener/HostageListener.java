package com.playminity.seawars.listener;

import com.playminity.minigameframework.modules.utils.GameTeam;
import com.playminity.seawars.Data;
import com.playminity.seawars.SWGameHandler;
import com.playminity.seawars.SeaWars;
import com.playminity.seawars.utils.SWStats;
import com.playminity.seawars.utils.teams.TeamItems;
import me.kombustorlp.gameapi.GameAPI;
import me.kombustorlp.gameapi.languagesystem.Language;
import me.kombustorlp.gameapi.libraries.ItemUtils;
import me.kombustorlp.gameapi.misc.User;
import me.kombustorlp.gameapi.utils.InvUtil;
import me.kombustorlp.gameapi.utils.ProtocolAPI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by André on 02.11.2016.
 */
public class HostageListener implements Listener {

    @EventHandler
    public void onDeath ( PlayerDeathEvent event ) {

        if ( SWGameHandler.getInstance( ).getCurrentPhase( ) != SeaWars.getInstance( ).getFightPhase( ) ) {
            return;
        }

        Player player = event.getEntity( );
        GameTeam gameTeam = SWGameHandler.getInstance( ).getTeamModule( ).getTeam( player );

        if ( player.getKiller( ) == null ) {

            if ( TeamItems.containsItem( player, Material.TRIPWIRE_HOOK ) ) {

                new BukkitRunnable( ) {
                    @Override
                    public void run ( ) {

                        TeamItems.giveRandomKey( );
                    }
                }.runTaskLater( SeaWars.getInstance( ), 40 );
            }

            return;
        }

        Player player1 = player.getKiller( );

        if ( gameTeam.getIndex( ) == 1 ) {

            if ( TeamItems.containsItem( player, Material.TRIPWIRE_HOOK ) ) {

                TeamItems.giveRandomKey( );
            }
        }

        if ( gameTeam.getIndex( ) == 2 ) {

            if ( TeamItems.containsItem( player, Material.TRIPWIRE_HOOK ) ) {

                User user = GameAPI.getUser( player1 );
                Language language = user.getLanguage( );
                user.getPlayer( ).getInventory( ).addItem( ItemUtils.createItemStack( Material.TRIPWIRE_HOOK, language.get( "items.pirates.key" ) ) );
                InvUtil.Update( user.getPlayer( ) );

                SWGameHandler.getInstance( ).setPirateUserWithKey( null );

                user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "items.pirates.key.received" );
            }

            int kills = SWGameHandler.getInstance( ).getPiratesKills( ) + 1;
            SWGameHandler.getInstance( ).setPiratesKills( kills );

            if ( kills == 10 || kills == 20 || kills == 30 || kills == 45 || kills == 60 || kills == 70 ) {

                Data.getVotedArena( ).getData( ).getWorld( ).dropItem( Data.getVotedArena( ).getData( ).getSpawn( "items", 1 ).getLocation( ), new ItemStack( Material.DIAMOND_CHESTPLATE ) );

                SWGameHandler.getInstance( ).setSailorsUpgrade( true );
                for ( Player player2 : SWGameHandler.getInstance( ).getTeam( 1 ).getPlayerList( ) ) {

                    GameAPI.getUser( player2 ).sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "sailors.upgrade.available" );
                    ProtocolAPI.sendActionbar( player2, GameAPI.getUser( player2 ).getLanguage( ).get( "sailors.upgrade.available" ) );
                }
            }
        }
    }

    @EventHandler
    public void onInteract ( PlayerInteractEvent event ) {

        if ( event.getAction( ) != Action.RIGHT_CLICK_BLOCK ) {

            return;
        }

        if ( ( event.getClickedBlock( ).getType( ) != Material.DARK_OAK_DOOR ) ) {

            return;
        }

        if ( SWGameHandler.getInstance( ).getCurrentPhase( ) != SeaWars.getInstance( ).getFightPhase( ) ) {

            event.setCancelled( true );
            return;
        }

        if ( SWGameHandler.getInstance( ).isHostage( ) == false ) {

            event.setCancelled( true );
            return;
        }

        User user = GameAPI.getUser( event.getPlayer( ) );

        if ( SWGameHandler.getInstance( ).getTeamModule( ).getTeam( user.getPlayer( ) ).getIndex( ) != 1 ) {

            event.setCancelled( true );
            return;
        }

        if ( user.getPlayer( ).getInventory( ).getItemInHand( ).getType( ) != Material.TRIPWIRE_HOOK ) {

            event.setCancelled( true );
            return;
        }

        event.setCancelled( true );

        for ( Player player : Bukkit.getOnlinePlayers( ) ) {

            Language language = GameAPI.getUser( player ).getLanguage( );

            ProtocolAPI.sendTitle( player, language.get( "hostage.delivered.title" ), language.get( "hostage.delivered.subtitle", user.getColoredName( ) ), 10, 40, 10 );
        }

        SWGameHandler.getInstance( ).setHostage( false );
        SWGameHandler.getInstance( ).getCurrentPhase( ).getScoreboard( ).update( );

        SeaWars.getStatsModule( ).add( user, SWStats.POINTS, 10 );
        user.addCoins( 10, true );

        user.getPlayer( ).getInventory( ).remove( Material.TRIPWIRE_HOOK );
        InvUtil.Update( user.getPlayer( ) );

        SWGameHandler.getInstance( ).getFakeNPC( ).despawn( );

        Language.broadcast( SWGameHandler.getInstance( ).getChatPrefix( ), "hostage.delivered.pirates.kill" );
    }
}
