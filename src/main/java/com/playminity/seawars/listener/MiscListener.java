package com.playminity.seawars.listener;

import com.playminity.seawars.SWGameHandler;
import com.playminity.seawars.SeaWars;
import com.playminity.seawars.utils.teams.TeamItems;
import me.kombustorlp.gameapi.GameAPI;
import me.kombustorlp.gameapi.misc.User;
import me.kombustorlp.gameapi.utils.ProtocolAPI;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by André on 14.10.2016.
 */
public class MiscListener implements Listener {

    @EventHandler
    public void onWeatherChange ( WeatherChangeEvent event ) {

        event.setCancelled( true );
    }

    @EventHandler
    public void onBlockBurn ( BlockBurnEvent event ) {

        event.setCancelled( true );
    }

    @EventHandler
    public void onEntityExplode ( EntityExplodeEvent event ) {

        event.blockList( ).clear( );
        event.setCancelled( true );
    }

    @EventHandler
    public void onFood ( FoodLevelChangeEvent event ) {

        event.setCancelled( true );
    }

    @EventHandler
    public void onItemDrop ( PlayerDropItemEvent event ) {

        event.setCancelled( true );
    }

    @EventHandler
    public void onAmorDamage ( EntityDamageEvent event ) {

        if ( ! ( event.getEntity( ) instanceof Player ) ) {

            return;
        }

        Player player = ( ( Player ) event.getEntity( ) );

        for ( ItemStack itemStack : player.getInventory( ).getArmorContents( ) ) {
            itemStack.setDurability( new ItemStack( itemStack.getType( ) ).getDurability( ) );
        }
    }

    @EventHandler
    public void onItemPickUp ( PlayerPickupItemEvent event ) {

        event.setCancelled( true );

        if ( SWGameHandler.getInstance( ).getCurrentPhase( ) != SeaWars.getInstance( ).getFightPhase( ) ) {
            return;
        }

        if ( event.getItem( ).getItemStack( ).getType( ) != Material.DIAMOND_CHESTPLATE ) {
            return;
        }

        Player player = event.getPlayer( );
        if ( SWGameHandler.getInstance( ).getTeamModule( ).getTeam( player ).getIndex( ) != 1 || SWGameHandler.getInstance( ).getTeamModule( ).getTeam( player ) == null ) {

            return;
        }

        event.getItem( ).remove( );

        if ( ! SWGameHandler.getInstance( ).isSailorsUpgrade( ) ) {

            return;
        }

        SWGameHandler.getInstance( ).setSailorsUpgrade( false );
        if ( TeamItems.isPercentageBetween( 10, 19, true, false ) ) SWGameHandler.getInstance( ).setLatestUpgrade( 1 );
        if ( TeamItems.isPercentageBetween( 20, 39, true, false ) ) SWGameHandler.getInstance( ).setLatestUpgrade( 2 );
        if ( TeamItems.isPercentageBetween( 40, 49, true, false ) ) SWGameHandler.getInstance( ).setLatestUpgrade( 3 );
        if ( TeamItems.isPercentageBetween( 50, 69, true, false ) ) SWGameHandler.getInstance( ).setLatestUpgrade( 4 );
        if ( TeamItems.isPercentageBetween( 70, 89, true, false ) ) SWGameHandler.getInstance( ).setLatestUpgrade( 5 );
        if ( TeamItems.isPercentageBetween( 90, 100, true, false ) ) SWGameHandler.getInstance( ).setLatestUpgrade( 6 );


        for ( Player player1 : SWGameHandler.getInstance( ).getTeam( 1 ).getPlayerList( ) ) {

            GameAPI.getUser( player1 ).sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "sailors.upgraded" );
            ProtocolAPI.sendActionbar( player1, GameAPI.getUser( player1 ).getLanguage( ).get( "sailors.upgraded" ) );
            TeamItems.giveTeamItems( player1 );
            player1.playSound( player1.getLocation( ), Sound.LEVEL_UP, 5, 5 );
        }
    }

    @EventHandler
    public void onRespawn ( PlayerRespawnEvent event ) {

        Player player = event.getPlayer( );

        TeamItems.giveTeamItems( player );
    }

    @EventHandler
    public void onInteract ( PlayerInteractEvent event ) {

        if ( event.getAction( ) == Action.PHYSICAL ) {

            if ( event.getClickedBlock( ).getType( ) == Material.WOOD_PLATE || event.getClickedBlock( ).getType( ) == Material.STONE_PLATE ||
                    event.getClickedBlock( ).getType( ) == Material.IRON_PLATE || event.getClickedBlock( ).getType( ) == Material.GOLD_PLATE ) {

                event.setCancelled( true );
            }
        }

        if ( event.getAction( ) != Action.RIGHT_CLICK_BLOCK ) {

            return;
        }

        if ( event.getClickedBlock( ).getType( ) != Material.GOLD_BLOCK.TRAP_DOOR || event.getClickedBlock( ).getType( ) != Material.CHEST ||
                event.getClickedBlock( ).getType( ) != Material.STONE_BUTTON || event.getClickedBlock( ).getType( ) != Material.WOOD_BUTTON ||
                event.getClickedBlock( ).getType( ) != Material.LEVER || event.getClickedBlock( ).getType( ) != Material.ENDER_CHEST ) {

            return;
        }

        event.setCancelled( true );
    }

    @EventHandler
    public void onMove ( PlayerMoveEvent event ) {

        Player player = event.getPlayer( );
        User user = GameAPI.getUser( player );

        if ( SeaWars.getInstance( ).getTutorialManager( ).isInTutorial( user ) ) {

            player.teleport( event.getFrom( ) );
        }
    }
}
