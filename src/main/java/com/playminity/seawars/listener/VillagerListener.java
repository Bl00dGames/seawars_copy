package com.playminity.seawars.listener;

import com.playminity.minigameframework.modules.utils.GameTeam;
import com.playminity.seawars.Data;
import com.playminity.seawars.SWGameHandler;
import com.playminity.seawars.SeaWars;
import com.playminity.seawars.utils.SWStats;
import com.playminity.seawars.utils.WorldUtil;
import lombok.Getter;
import me.kombustorlp.gameapi.GameAPI;
import me.kombustorlp.gameapi.languagesystem.Language;
import me.kombustorlp.gameapi.utils.ProtocolAPI;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Map;

/**
 * Created by André on 30.10.2016.
 */
public class VillagerListener implements Listener {

    @Getter
    public int villager = 3;

    @EventHandler
    public void onEntityDamage ( EntityDamageEvent event ) {

        if ( SWGameHandler.getInstance( ).getCurrentPhase( ) != SeaWars.getInstance( ).getFightPhase( ) ) {

            return;
        }

        if ( ! event.getEntityType( ).equals( EntityType.VILLAGER ) ) {
            return;
        }

        if ( ! event.getCause( ).equals( EntityDamageEvent.DamageCause.ENTITY_ATTACK ) ) {
            event.setCancelled( true );
        }
    }

    @EventHandler
    public void onEntityDamageByEntity ( EntityDamageByEntityEvent event ) {

        if ( SWGameHandler.getInstance( ).getCurrentPhase( ) != SeaWars.getInstance( ).getFightPhase( ) ) {

            event.setCancelled( true );
            return;
        }

        if ( ! event.getEntityType( ).equals( EntityType.VILLAGER ) ) {
            return;
        }

        if ( ! ( event.getDamager( ) instanceof Player ) ) {

            event.setCancelled( true );
        }

        if ( event.getDamager( ).getType( ) == EntityType.SNOWBALL ) {

            return;
        }

        if ( ChatColor.stripColor( event.getEntity( ).getCustomName( ) ).equalsIgnoreCase( "Marc" ) ) {

            event.setCancelled( true );
            return;
        }


        Player player = ( ( Player ) event.getDamager( ) );
        GameTeam gameTeam = SWGameHandler.getInstance( ).getTeamModule( ).getTeam( player );

        if ( gameTeam.getIndex( ) == 1 ) {

            event.setCancelled( true );
            return;
        }

        if ( SWGameHandler.getInstance( ).getTeam( 1 ).getPlayerList( ).size( ) != 0 ) {
            for ( Player player1 : SWGameHandler.getInstance( ).getTeam( 1 ).getPlayerList( ) ) {

                ProtocolAPI.sendActionbar( player1, GameAPI.getUser( player1 ).getLanguage( ).get( "villager.attack" ) );
            }
        }
    }

    @EventHandler
    public void onEntityDeath ( EntityDeathEvent event ) {

        if ( ! event.getEntityType( ).equals( EntityType.VILLAGER ) ) {
            return;
        }

        villager--;

        SWGameHandler.getInstance( ).setVillager( villager );

        SeaWars.getInstance( ).getFightPhase( ).getScoreboard( ).update( );

        Language.broadcast( SWGameHandler.getInstance( ).getChatPrefix( ), "villager.death.title" );
        if ( villager == 0 ) {
            Language.broadcast( SWGameHandler.getInstance( ).getChatPrefix( ), "villager.death.subtitle.0" );
        } else {
            Language.broadcast( SWGameHandler.getInstance( ).getChatPrefix( ), "villager.death.subtitle", villager );
        }

        for ( Player player : Bukkit.getOnlinePlayers( ) ) {

            Language language = GameAPI.getUser( player ).getLanguage( );

            if ( villager == 0 ) {
                ProtocolAPI.sendTitle( player, language.get( "villager.death.title" ), language.get( "villager.death.subtitle.0" ), 10, 40, 10 );
                continue;
            }

            ProtocolAPI.sendTitle( player, language.get( "villager.death.title" ), language.get( "villager.death.subtitle", villager ), 10, 40, 10 );
        }

        SeaWars.getStatsModule( ).add( GameAPI.getUser( event.getEntity( ).getKiller( ) ), SWStats.POINTS, 10 );
        GameAPI.getUser( event.getEntity( ).getKiller( ) ).addCoins( 10, true );

        new BukkitRunnable( ) {
            @Override
            public void run ( ) {

                for ( Map.Entry< Location, Block > entry : WorldUtil.getBlocksOfMaterial( event.getEntity( ).getLocation( ), 8, Material.IRON_FENCE ).entrySet( ) ) {

                    Block block = Data.getVotedArena( ).getData( ).getWorld( ).getBlockAt( entry.getKey( ) );

                    if ( block.getType( ) == Material.AIR ) {
                        cancel( );
                    }

                    entry.getKey( ).getWorld( ).spigot( ).playEffect( entry.getKey( ), Effect.SMOKE );

                }
            }
        }.runTaskTimerAsynchronously( SeaWars.getInstance( ), 0, 20 );


        new BukkitRunnable( ) {

            @Override
            public void run ( ) {

                for ( Map.Entry< Location, Block > entry : WorldUtil.getBlocksOfMaterial( event.getEntity( ).getLocation( ), 8, Material.IRON_FENCE ).entrySet( ) ) {

                    Block block = Data.getVotedArena( ).getData( ).getWorld( ).getBlockAt( entry.getKey( ) );
                    block.setType( Material.AIR );
                }

            }
        }.runTaskLater( SeaWars.getInstance( ), 5 * 20 );
    }
}
