package com.playminity.seawars.listener;

import com.playminity.seawars.Data;
import com.playminity.seawars.SWGameHandler;
import com.playminity.seawars.SeaWars;
import com.playminity.seawars.arenasystem.SWArenaHandler;
import com.playminity.seawars.utils.SWStats;
import com.playminity.seawars.utils.WorldUtil;
import com.playminity.seawars.utils.teams.TeamItems;
import me.kombustorlp.gameapi.GameAPI;
import me.kombustorlp.gameapi.languagesystem.Language;
import me.kombustorlp.gameapi.libraries.ItemUtils;
import me.kombustorlp.gameapi.misc.User;
import me.kombustorlp.gameapi.utils.InvUtil;
import me.kombustorlp.gameapi.utils.ProtocolAPI;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by André on 01.11.2016.
 */
public class GoldCollectListener implements Listener {

    private List< Player > players = new ArrayList<>( );
    private List< Player > playerMessage = new ArrayList<>( );

    @EventHandler
    public void onInteract ( PlayerInteractEvent event ) {

        Player player = event.getPlayer( );
        User user = GameAPI.getUser( player );

        if ( SWGameHandler.getInstance( ).getCurrentPhase( ) != SeaWars.getInstance( ).getFightPhase( ) ) {
            return;
        }

        if ( SWGameHandler.getInstance( ).getTeamModule( ).getTeam( player ) == null ) {

            return;
        }

        if ( SWGameHandler.getInstance( ).getTeamModule( ).getTeam( player ).getIndex( ) != 2 ) {

            return;
        }

        if ( event.getAction( ) != Action.RIGHT_CLICK_BLOCK ) {
            return;
        }

        Block block = event.getClickedBlock( );

        if ( block.getType( ) != Material.GOLD_BLOCK ) {

            return;
        }

        if ( players.contains( player ) ) {

            if ( playerMessage.contains( player ) ) {
                return;
            }

            user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "gold.collecting" );
            playerMessage.add( player );

            return;
        }

        block.setType( Material.AIR );

        for ( Player player1 : Bukkit.getOnlinePlayers( ) ) {

            ProtocolAPI.sendActionbar( player1, GameAPI.getUser( player1 ).getLanguage( ).get( "gold.collected", GameAPI.getUser( player ).getColoredName( ) ) );
        }

        players.add( player );

        SWArenaHandler.getInstance( ).getBlockList( ).remove( block.getLocation( ) );
        SWArenaHandler.getInstance( ).getBlockList( ).put( block.getLocation( ), block );

        player.getInventory( ).addItem( ItemUtils.createItemStack( Material.GOLD_BLOCK, 1, "§eGold" ) );

        double amount = 0;

        if ( player.getInventory( ).contains( Material.GOLD_BLOCK ) ) {

            for ( ItemStack itemStack : player.getInventory( ).getContents( ) ) {

                if ( itemStack == null ) {
                    continue;
                }

                if ( itemStack.getType( ) != Material.GOLD_BLOCK ) {
                    continue;
                }

                amount = itemStack.getAmount( );
            }
        }

        float walk_speed = ( float ) ( 0.2 / amount );

        player.setWalkSpeed( walk_speed );

        new BukkitRunnable( ) {

            int i = 1;

            @Override
            public void run ( ) {

                if ( i == 5 ) {

                    user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "gold.collect" );
                    cancel( );
                }
                ProtocolAPI.sendActionbar( player, getLoadMessage( i ) );
                i++;
            }
        }.runTaskTimer( SeaWars.getInstance( ), 20, 20 );

        new BukkitRunnable( ) {

            @Override
            public void run ( ) {

                players.remove( player );
                playerMessage.remove( player );
            }
        }.runTaskLater( SeaWars.getInstance( ), 5 * 20 );
    }

    @EventHandler
    public void onDeath ( PlayerDeathEvent event ) {

        Player player = event.getEntity( );

        if ( player.getKiller( ) instanceof Player ) {

            SeaWars.getStatsModule( ).add( GameAPI.getUser( player.getKiller( ) ), SWStats.POINTS, 5 );
            GameAPI.getUser( player.getKiller( ) ).addCoins( 3, true );
        }

        int amount = 0;

        if ( player.getInventory( ).contains( Material.GOLD_BLOCK ) ) {

            for ( ItemStack itemStack : player.getInventory( ).getContents( ) ) {

                if ( itemStack == null ) {
                    continue;
                }

                if ( itemStack.getType( ) != Material.GOLD_BLOCK ) {
                    continue;
                }

                amount = itemStack.getAmount( );
            }
        }

        for ( Map.Entry< Location, Block > entry : SWArenaHandler.getInstance( ).getBlockList( ).entrySet( ) ) {

            if ( amount == 0 ) {

                break;
            }

            if ( entry.getValue( ).getType( ) != Material.AIR ) {
                continue;
            }

            amount--;
            Block block = Data.getVotedArena( ).getData( ).getWorld( ).getBlockAt( entry.getKey( ) );
            block.setType( Material.GOLD_BLOCK );
            entry.setValue( block );
        }

        player.setWalkSpeed( ( float ) 0.2 );
    }

    @EventHandler( priority = EventPriority.HIGH )
    public void onPlayerInteractEntity ( PlayerInteractEntityEvent event ) {

        if ( SWGameHandler.getInstance( ).getCurrentPhase( ) != SeaWars.getInstance( ).getFightPhase( ) ) {

            event.setCancelled( true );
            return;
        }

        if ( event.getRightClicked( ).getType( ) != EntityType.VILLAGER ) return;

        event.setCancelled( true );

        if ( ! ChatColor.stripColor( event.getRightClicked( ).getCustomName( ) ).equalsIgnoreCase( "Marc" ) ) return;

        Player player = event.getPlayer( );
        User user = GameAPI.getUser( player );

        if ( SWGameHandler.getInstance( ).getTeamModule( ).getTeam( player ).getIndex( ) != 2 || SWGameHandler.getInstance( ).getTeamModule( ).getTeam( player ) == null ) {

            return;
        }

        if ( player.getItemInHand( ).getType( ) != Material.GOLD_BLOCK ) {

            user.sendMessage( SWGameHandler.getInstance( ).getChatPrefix( ), "villager.pirates.trade.goldinhand" );
            return;
        }

        int amount = 0;

        for ( ItemStack itemStack : player.getInventory( ).getContents( ) ) {

            if ( itemStack == null ) {
                continue;
            }

            if ( itemStack.getType( ) != Material.GOLD_BLOCK ) {
                continue;
            }

            amount += itemStack.getAmount( );
        }

        player.getInventory( ).remove( Material.GOLD_BLOCK );
        InvUtil.Update( player );

        Language.broadcast( SWGameHandler.getInstance( ).getChatPrefix( ), "gold.saved", user.getColoredName( ) );

        double goldCollected0 = SWArenaHandler.getInstance( ).getSelectedGold( );
        double goldCollected1 = goldCollected0 + amount;

        SeaWars.getStatsModule( ).add( user, SWStats.GOLD, amount );
        SeaWars.getStatsModule( ).add( user, SWStats.POINTS, 20 );
        user.addCoins( 20, true );

        SWArenaHandler.getInstance( ).setSelectedGold( goldCollected1 );
        SeaWars.getInstance( ).getFightPhase( ).getScoreboard( ).update( );

        if ( WorldUtil.calculatePercent( goldCollected1 ) == 100 ) {

            Language.broadcast( SWGameHandler.getInstance( ).getChatPrefix( ), "gold.collected.all" );
            Bukkit.getOnlinePlayers( ).forEach( player1 -> player1.playSound( player1.getLocation( ), Sound.GHAST_DEATH, 5, 5 ) );
        }

        if ( ( WorldUtil.calculatePercent( goldCollected0 ) < 10 && WorldUtil.calculatePercent( goldCollected1 ) >= 10 ) ||
                ( WorldUtil.calculatePercent( goldCollected0 ) < 20 && WorldUtil.calculatePercent( goldCollected1 ) >= 20 ) ||
                ( WorldUtil.calculatePercent( goldCollected0 ) < 40 && WorldUtil.calculatePercent( goldCollected1 ) >= 40 ) ||
                ( WorldUtil.calculatePercent( goldCollected0 ) < 60 && WorldUtil.calculatePercent( goldCollected1 ) >= 60 ) ||
                ( WorldUtil.calculatePercent( goldCollected0 ) < 80 && WorldUtil.calculatePercent( goldCollected1 ) >= 80 ) ||
                ( WorldUtil.calculatePercent( goldCollected0 ) < 100 && WorldUtil.calculatePercent( goldCollected1 ) >= 100 ) ) {

            for ( Player player1 : SWGameHandler.getInstance( ).getTeam( 2 ).getPlayerList( ) ) {

                ProtocolAPI.sendActionbar( player1, GameAPI.getUser( player1 ).getLanguage( ).get( "items.pirates.upgrade" ) );

                TeamItems.giveTeamItems( player1 );
            }
        }

        player.setWalkSpeed( 0.2F );
    }

    private String getLoadMessage ( int i ) {

        String message = "§e";

        for ( int j = 1; j <= i; j++ ) {

            message += "⬛⬛";
        }

        message += "§8";

        for ( int j = 1; j <= ( 5 - i ); j++ ) {
            message += "⬛⬛";
        }

        return message;
    }
}
