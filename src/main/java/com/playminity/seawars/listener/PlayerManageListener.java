package com.playminity.seawars.listener;

import com.playminity.seawars.SWGameHandler;
import com.playminity.seawars.SeaWars;
import com.playminity.seawars.arenasystem.SWArenaHandler;
import com.playminity.seawars.utils.SWStats;
import com.playminity.seawars.utils.WorldUtil;
import me.kombustorlp.gameapi.GameAPI;
import me.kombustorlp.gameapi.misc.User;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

/**
 * Created by André on 01.11.2016.
 */
public class PlayerManageListener implements Listener {

    @EventHandler
    public void onQuit ( PlayerQuitEvent event ) {

        Player player = event.getPlayer( );

        if ( SWGameHandler.getInstance( ).getTeamModule( ).getTeam( player ) == null ) return;

        SWGameHandler.getInstance( ).getTeamModule( ).getTeam( player ).leave( player );
        if ( SWGameHandler.getInstance( ).getCurrentPhase( ).getScoreboard( ) != null )
            SWGameHandler.getInstance( ).getCurrentPhase( ).getScoreboard( ).update( );
    }

    @EventHandler
    public void onKick ( PlayerKickEvent event ) {

        Player player = event.getPlayer( );

        if ( SWGameHandler.getInstance( ).getTeamModule( ).getTeam( player ) == null ) return;

        SWGameHandler.getInstance( ).getTeamModule( ).getTeam( event.getPlayer( ) ).leave( player );
        if ( SWGameHandler.getInstance( ).getCurrentPhase( ).getScoreboard( ) != null )
            SWGameHandler.getInstance( ).getCurrentPhase( ).getScoreboard( ).update( );
    }

    @EventHandler
    public void onRespawnEvent ( PlayerRespawnEvent event ) {

        Player player = event.getPlayer( );

        if ( SWGameHandler.getInstance( ).getCurrentPhase( ) != SeaWars.getInstance( ).getFightPhase( ) ) {

            return;
        }

        if ( SWGameHandler.getInstance( ).getTeamModule( ).getTeam( player ).getIndex( ) == 1 ) {

            if ( WorldUtil.calculatePercent( SWArenaHandler.getInstance( ).getSelectedGold( ) ) == 100 ) {

                SWGameHandler.getInstance( ).outOfGameDeath( player, GameAPI.getUser( player ) );
                SWGameHandler.getInstance( ).setSpectator( player );
                player.setScoreboard( Bukkit.getScoreboardManager( ).getNewScoreboard( ) );
            }
        }

        if ( SWGameHandler.getInstance( ).getTeamModule( ).getTeam( player ).getIndex( ) == 2 ) {

            if ( SWGameHandler.getInstance( ).isHostage( ) == false ) {

                SWGameHandler.getInstance( ).outOfGameDeath( player, GameAPI.getUser( player ) );
                SWGameHandler.getInstance( ).setSpectator( player );
                player.setScoreboard( Bukkit.getScoreboardManager( ).getNewScoreboard( ) );
            }
        }

        SWGameHandler.getInstance( ).getSpectateModule( ).update( );
        SeaWars.getInstance( ).getFightPhase( ).getScoreboard( ).update( );

        player.setScoreboard( SeaWars.getInstance( ).getFightPhase( ).getScoreboard( ).getScoreboard( GameAPI.getUser( player ).getLanguage( ) ) );

        if ( SWGameHandler.getInstance( ).getTeam( 1 ) == null || SWGameHandler.getInstance( ).getTeam( 1 ).getPlayerList( ).size( ) == 0 ) {

            for ( Player player1 : SWGameHandler.getInstance( ).getTeam( 2 ).getPlayerList( ) ) {

                User user = GameAPI.getUser( player1 );

                SeaWars.getStatsModule( ).add( user, SWStats.POINTS, 100 );
                user.addCoins( 50, true );
            }
            SWGameHandler.getInstance( ).winner( SWGameHandler.getInstance( ).getTeam( 2 ) );
        }

        if ( SWGameHandler.getInstance( ).getTeam( 2 ) == null || SWGameHandler.getInstance( ).getTeam( 2 ).getPlayerList( ).size( ) == 0 ) {

            for ( Player player1 : SWGameHandler.getInstance( ).getTeam( 1 ).getPlayerList( ) ) {

                User user = GameAPI.getUser( player1 );

                SeaWars.getStatsModule( ).add( user, SWStats.POINTS, 100 );
                user.addCoins( 50, true );
            }
            SWGameHandler.getInstance( ).winner( SWGameHandler.getInstance( ).getTeam( 1 ) );
        }
    }
}
